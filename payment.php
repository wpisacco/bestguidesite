<!DOCTYPE html>
<html lang="en">

<head>
      <?php require 'header.php';?>
</head>

<header>
    <div id="top_line">
        <?php require 'header1.php';?>
    </div>
         <?php require 'header2.php';?>  
</header>

<body>

	<section id="hero_3">
		<div class="intro_title">
			<h1>Preparando el Pago</h1>
			<div class="bs-wizard row">

                <div class="col-4 bs-wizard-step active">
                    <div class="text-center bs-wizard-stepnum">Selección de Servicios</div>
                    <div class="progress">
                        <div class="progress-bar"></div>
                    </div>
                    <a href="cart.html" class="bs-wizard-dot"></a>
                </div>

                <div class="col-4 bs-wizard-step active">
                    <div class="text-center bs-wizard-stepnum">Detalle del Pago</div>
                    <div class="progress">
                        <div class="progress-bar"></div>
                    </div>
                    <a href="payment.html" class="bs-wizard-dot"></a>
                </div>

                <div class="col-4 bs-wizard-step disabled">
                    <div class="text-center bs-wizard-stepnum">Finalizar!</div>
                    <div class="progress">
                        <div class="progress-bar"></div>
                    </div>
                    <a href="confirmation.html" class="bs-wizard-dot"></a>
                </div>

			</div>
			<!-- End bs-wizard -->
		</div>
		<!-- End intro-title -->
	</section>
	<!-- End Section hero_2 -->

	<main>
        <div id="position">
            <div class="container">
                <ul>
                </ul>
            </div>
        </div>
		<!-- End position -->

		<div class="container margin_60">
			<div class="row">
				<div class="col-lg-8 add_bottom_15">
					<div class="form_title">
						<h3><strong>1</strong>Datos personales</h3>
						<p>
							Mussum ipsum cacilds, vidis litro abertis.
						</p>
					</div>
					<div class="step">
						<div class="row">
							<div class="col-sm-6">
								<div class="form-group">
									<label>Nombre</label>
									<input type="text" class="form-control" id="firstname_booking" name="firstname_booking">
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group">
									<label>Apellido</label>
									<input type="text" class="form-control" id="lastname_booking" name="lastname_booking">
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-6">
								<div class="form-group">
									<label>Email</label>
									<input type="email" id="email_booking" name="email_booking" class="form-control">
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group">
									<label>Confirmar Email</label>
									<input type="email" id="email_booking_2" name="email_booking_2" class="form-control">
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-6">
								<div class="form-group">
									<label>Telefono</label>
									<input type="text" id="telephone_booking" name="telephone_booking" class="form-control">
								</div>
							</div>
						</div>
					</div>
					<!--End step -->

					<div class="form_title">
						<h3><strong>2</strong>Información del Pago</h3>
						<p>
							ingrese los datos tal como figuran el la tarjeta.
						</p>
					</div>
					<div class="step">
						<div class="form-group">
							<label>Nombre Como Figura en la tarjeta</label>
							<input type="text" class="form-control" id="name_card_bookign" name="name_card_bookign">
						</div>
						<div class="row">
							<div class="col-md-6 col-sm-12">
								<div class="form-group">
									<label>Numero de Tarjeta</label>
									<input type="text" id="card_number" name="card_number" class="form-control">
								</div>
							</div>
							<div class="col-md-6 col-sm-12">
								<img src="img/cards.png" width="207" height="43" alt="Cards" class="cards">
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<label>Feecha de Expiración</label>
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<input type="text" id="expire_month" name="expire_month" class="form-control" placeholder="MM">
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<input type="text" id="expire_year" name="expire_year" class="form-control" placeholder="Año">
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>Código de Seguridad</label>
									<div class="row">
										<div class="col-4">
											<div class="form-group">
												<input type="text" id="ccv" name="ccv" class="form-control" placeholder="CCV">
											</div>
										</div>
										<div class="col-8">
											<img src="img/icon_ccv.gif" width="50" height="29" alt="ccv"><small>Últimos 3 digitos</small>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!--End row -->

						<hr>

						<h4>O Pague con Paypal</h4>
						<p>
							Lorem ipsum dolor sit amet, vim id accusata sensibus, id ridens quaeque qui. Ne qui vocent ornatus molestie, reque fierent dissentiunt mel ea.
						</p>
						<p>
							<img src="img/paypal_bt.png" alt="Image">
						</p>
					</div>
					<!--End step -->

					<div class="form_title">
						<h3><strong>3</strong>Dirección de Facturación</h3>
						<p>
							Mussum ipsum cacilds, vidis litro abertis.
						</p>
					</div>
					<div class="step">
						<div class="row">
							<div class="col-sm-6">
								<div class="form-group">
									<label>Pais</label>
									<select class="form-control" name="country" id="country">
										<option value="" selected>Seleccione su pais</option>
										<option value="Europe">Europe</option>
										<option value="United states">United states</option>
										<option value="Asia">Asia</option>
									</select>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-6">
								<div class="form-group">
									<label>Calle linea 1</label>
									<input type="text" id="street_1" name="street_1" class="form-control">
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group">
									<label>Calle linea 2</label>
									<input type="text" id="street_2" name="street_2" class="form-control">
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6 col-sm-12">
								<div class="form-group">
									<label>Ciudad</label>
									<input type="text" id="city_booking" name="city_booking" class="form-control">
								</div>
							</div>
							<div class="col-md-3 col-sm-6">
								<div class="form-group">
									<label>Estado / Pcia</label>
									<input type="text" id="state_booking" name="state_booking" class="form-control">
								</div>
							</div>
							<div class="col-md-3 col-sm-6">
								<div class="form-group">
									<label>Código Postal</label>
									<input type="text" id="postal_code" name="postal_code" class="form-control">
								</div>
							</div>
						</div>
						<!--End row -->
					</div>
					<!--End step -->

					<div id="policy">
						<h4>Políticas de Cancelación</h4>
						<div class="form-group">
							<label>
								<input type="checkbox" name="policy_terms" id="policy_terms">Acepto los terminos y condiciones y politicas en general.</label>
						</div>
						<a href="confirmation.php" class="btn_1 green medium">Pagar</a>
					</div>
				</div>

				<aside class="col-lg-4">
					<div class="box_style_1">
						<h3 class="inner">- Summary -</h3>
						<table class="table table_summary">
							<tbody>
								<tr>
									<td>
										Adults
									</td>
									<td class="text-right">
										2
									</td>
								</tr>
								<tr>
									<td>
										Children
									</td>
									<td class="text-right">
										0
									</td>
								</tr>
								<tr>
									<td>
										Dedicated tour guide
									</td>
									<td class="text-right">
										$34
									</td>
								</tr>
								<tr>
									<td>
										Insurance
									</td>
									<td class="text-right">
										$34
									</td>
								</tr>
								<tr class="total">
									<td>
										Total cost
									</td>
									<td class="text-right">
										$154
									</td>
								</tr>
							</tbody>
                           </table> 
                            <a class="btn_full" href="confirmation.php">Pagar</a>

					</div>
					<div class="box_style_4">
                        <?php require 'telefono.php';?>
					</div>
				</aside>

			</div>
			<!--End row -->
		</div>
		<!--End container -->
	</main>
	<!-- End main -->
            <?php require 'footer.php';?>

</body>

</html>