<!DOCTYPE html>
<html lang="en">

<head>
      <?php require 'header.php';?>
</head>

<header>
     
<div id="top_line">
        <?php require 'header1.php';?>
    </div>
         <?php require 'header2.php';?>     
</header>
 <body>  
    <section id="hero_1">
		<div class="intro_title">
			<h1>Preparando tu orden</h1>
			<div class="bs-wizard row">

				<div class="col-4 bs-wizard-step active">
					<div class="text-center bs-wizard-stepnum">Selección de Servicios</div>
					<div class="progress">
						<div class="progress-bar"></div>
					</div>
					<a href="cart.html" class="bs-wizard-dot"></a>
				</div>

				<div class="col-4 bs-wizard-step disabled">
					<div class="text-center bs-wizard-stepnum">Detalle del Pago</div>
					<div class="progress">
						<div class="progress-bar"></div>
					</div>
					<a href="payment.html" class="bs-wizard-dot"></a>
				</div>

				<div class="col-4 bs-wizard-step disabled">
					<div class="text-center bs-wizard-stepnum">Finalizar!</div>
					<div class="progress">
						<div class="progress-bar"></div>
					</div>
					<a href="confirmation.html" class="bs-wizard-dot"></a>
				</div>

			</div>
			<!-- End bs-wizard -->
		</div>
		<!-- End intro-title -->
	</section>
	<!-- End Section hero_2 -->

	<main>
        <div id="position">
            <div class="container">
                <ul>
                </ul>
            </div>
        </div>
		<!-- End position -->

		<div class="container margin_60">
			<div class="row">
				<div class="col-lg-8">
					<table class="table table-striped options_cart">
						<thead>
							<tr>
								<th colspan="3">
									Agregar / Servicios
								</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td style="width:10%">
									<i class="icon_set_1_icon-26"></i>
								</td>
								<td style="width:60%">
									Transfer al Aeropuerto <strong>+ US$ 34</strong>
								</td>
								<td style="width:35%">
									<label class="switch-light switch-ios float-right">
										<input type="checkbox" name="option_1" id="option_1" value="">
										<span>
                    					<span>No</span>
										<span>Yes</span>
										</span>
										<a></a>
									</label>
								</td>
							</tr>
							<tr>
								<td>
									<i class="icon_set_1_icon-40"></i>
								</td>
								<td>
									Alquiler de Bicicleta <strong>+ US$ 26*</strong>
								</td>
								<td>
									<label class="switch-light switch-ios float-right">
										<input type="checkbox" name="option_7" id="option_7" value="">
										<span>
                    					<span>No</span>
										<span>Yes</span>
										</span>
										<a></a>
									</label>
								</td>
							</tr>
						</tbody>
					</table>
					<div class="add_bottom_15"><small>* Pricio por persona.</small>
					</div>
				</div>
				<!-- End col -->

				<aside class="col-lg-4">
					<div class="box_style_1">
						<h3 class="inner">- Resumen -</h3>
                        <table class="table table_summary">
                            <tbody>
                                <tr>
                                    <td>
                                        Adultos
                                    </td>
                                    <td class="text-right">
                                        2
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Niños
                                    </td>
                                    <td class="text-right">
                                        0
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Costo del Servicio
                                    </td>
                                    <td class="text-right">
                                        3x US$ 8
                                    </td>
                                </tr>
                                <tr class="total">
                                    <td>
                                        Costo Total
                                    </td>
                                    <td class="text-right">
                                        US$ 30 (*)
                                    </td>
                                </tr>
                            </tbody>
                        </table>
						<a class="btn_full" href="payment.php">Continuar con el Pago</a>
						<a class="btn_full_outline" href="javascript:history.back();"><i class="icon-right"></i> Modificar Orden</a>
					</div>
					<div class="box_style_4">
                        <?php require 'telefono.php';?>
					</div>
				</aside>
				<!-- End aside -->

			</div>
			<!--End row -->
		</div>
		<!--End container -->
	</main>
	<!-- End main -->

            <?php require 'footer.php';?>

</body>

</html>

