<!DOCTYPE html>
<html lang="en">

<head>
      <?php require 'header.php';?>
</head>

<header>
    <div id="top_line">
        <?php require 'header1.php';?>
    </div>
         <?php require 'header2.php';?>  
</header>

	<section class="parallax-window" data-parallax="scroll" data-image-src="img/header_bg.jpg" data-natural-width="1400" data-natural-height="470">
		<div class="parallax-content-1">
			<div class="animated fadeInDown">
				<h1>Contáctenos</h1>
				<p>Ridiculus sociosqu cursus neque cursus curae ante scelerisque vehicula.</p>
			</div>
		</div>
	</section>
	<!-- End Section -->

	<main>
		<div id="position">
			<div class="container">
			</div>
		</div>
		<!-- End Position -->

		<div class="container margin_60">
			<div class="row">
				<div class="col-md-8">
					<div class="form_title">
						<h3><strong><i class="icon-pencil"></i></strong>Complete el Formulario</h3>
						<p>
							Mussum ipsum cacilds, vidis litro abertis.
						</p>
					</div>
					<div class="step">

						<div id="message-contact"></div>
						<form method="post" action="assets/contact.php" id="contactform">
							<div class="row">
								<div class="col-sm-6">
									<div class="form-group">
										<label>Nombre</label>
										<input type="text" class="form-control" id="name_contact" name="name_contact" placeholder="Ingrese Nombre">
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<label>Apellido</label>
										<input type="text" class="form-control" id="lastname_contact" name="lastname_contact" placeholder="Ingrese Apellido">
									</div>
								</div>
							</div>
							<!-- End row -->
							<div class="row">
								<div class="col-sm-6">
									<div class="form-group">
										<label>Email</label>
										<input type="email" id="email_contact" name="email_contact" class="form-control" placeholder="Ingrese Email">
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<label>Teléfono</label>
										<input type="text" id="phone_contact" name="phone_contact" class="form-control" placeholder="Ingrese Número de Teléfono">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-12">
									<div class="form-group">
										<label>Mensaje</label>
										<textarea rows="5" id="message_contact" name="message_contact" class="form-control" placeholder="Escriba su mensaje" style="height:200px;"></textarea>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-6">
									<label>Verification Humana</label>
									<input type="text" id="verify_contact" class=" form-control add_bottom_30" placeholder="Cual es la respuesta? 3 + 1 =">
									<input type="submit" value="Enviar" class="btn_1" id="submit-contact">
								</div>
							</div>
						</form>
					</div>
				</div>
				<!-- End col-md-8 -->

				<div class="col-md-4">
					<div class="box_style_1">
						<span class="tape"></span>
						<h4>Dirección  <span><i class="icon-pin pull-right"></i></span></h4>
						<p>
							Place Charles de Gaulle, 75008 Paris
						</p>
						<hr>
						<h4>Centro de Ayuda <span><i class="icon-help pull-right"></i></span></h4>
						<p>
							Lorem ipsum dolor sit amet, vim id accusata sensibus, id ridens quaeque qui. Ne qui vocent ornatus molestie.
						</p>
						<ul id="contact-info">
							<li>+ 54 (11) 65309188 / + 54 (11) 65309189</li>
							<li><a href="#">info@bestguide.com.ar</a>
							</li>
						</ul>
					</div>
                    <div class="box_style_4">
                        <?php require 'telefono.php';?>
                    </div>
				</div>
				<!-- End col-md-4 -->
			</div>
			<!-- End row -->
		</div>
		<!-- End container -->

	</main>
	<!-- End main -->

    <!-- End main -->
            <?php require 'footer.php';?>

</body>

</html>                              
	<script src="js/map_contact.js"></script>
	<script src="js/infobox.js"></script>
	
