<!DOCTYPE html>
<html lang="en">

<head>
      <?php require 'header.php';?>
</head>

<header>
    <div id="top_line">
        <?php require 'header1.php';?>
    </div>
         <?php require 'header2.php';?>  
</header>

<body>

	<section id="hero_2">
		<div class="intro_title">
			<h1>Hemos Confirmado su Pago</h1>
			<div class="bs-wizard row">

                <div class="col-4 bs-wizard-step active">
                    <div class="text-center bs-wizard-stepnum">Selección de Servicios</div>
                    <div class="progress">
                        <div class="progress-bar"></div>
                    </div>
                    <a href="cart.html" class="bs-wizard-dot"></a>
                </div>

                <div class="col-4 bs-wizard-step active">
                    <div class="text-center bs-wizard-stepnum">Detalle del Pago</div>
                    <div class="progress">
                        <div class="progress-bar"></div>
                    </div>
                    <a href="payment.html" class="bs-wizard-dot"></a>
                </div>

                <div class="col-4 bs-wizard-step active">
                    <div class="text-center bs-wizard-stepnum">Finalizado!</div>
                    <div class="progress">
                        <div class="progress-bar"></div>
                    </div>
                    <a href="confirmation.html" class="bs-wizard-dot"></a>
                </div>

			</div>
			<!-- End bs-wizard -->
		</div>
		<!-- End intro-title -->
	</section>
	<!-- End Section hero_2 -->

	<main>
        <div id="position">
            <div class="container">
                <ul>
                </ul>
            </div>
        </div>
		<!-- End position -->

		<div class="container margin_60">
			<div class="row">
				<div class="col-lg-8 add_bottom_15">

					<div class="form_title">
						<h3><strong><i class="icon-ok"></i></strong>Gracias!</h3>
						<p>
							Mussum ipsum cacilds, vidis litro abertis.
						</p>
					</div>
					<div class="step">
						<p>
							Lorem ipsum dolor sit amet, nostrud nominati vis ex, essent conceptam eam ad. Cu etiam comprehensam nec. Cibo delicata mei an, eum porro legere no. Te usu decore omnium, quem brute vis at, ius esse officiis legendos cu. Dicunt voluptatum at cum. Vel et facete equidem deterruisset, mei graeco cetero labores et. Accusamus inciderint eu mea.
						</p>
					</div>
					<!--End step -->

					<div class="form_title">
						<h3><strong><i class="icon-tag-1"></i></strong>Resumen del Servicio</h3>
						<p>
							Mussum ipsum cacilds, vidis litro abertis.
						</p>
					</div>
					<div class="step">
						<table class="table table-striped confirm">
							<tbody>
                                <tr>
                                    <td>
                                        <strong>Lugar a Visitar</strong>
                                    </td>
                                    <td>
                                        Buenos Aires, Argentina
                                    </td>
                                </tr>                                
								<tr>
									<td>
										<strong>Guía</strong>
									</td>
									<td>
										Cecilia Pisaco
									</td>
								</tr>
                                <tr>
                                    <td><strong>Email</strong>
                                    </td>
                                    <td>
                                        cecilia1996@hotmail.com
                                        <br>
                                    </td>
                                </tr>
                                <tr>
                                    <td><strong>Teléfono</strong>
                                    </td>
                                    <td>
                                        +54 1165309188 / +54 11 44594999
                                        <br>
                                    </td>
                                </tr> 
								<tr>
									<td>
										<strong>Fecha</strong>
									</td>
									<td>
										10 Abril 2019</td>
								</tr>
								<tr>
									<td><strong>Adultos</strong>
									</td>
									<td>2</td>
								</tr>
								<tr>
									<td><strong>Niños</strong>
									</td>
									<td>1</td>
								</tr>
								<tr>
									<td>
										<strong>Tipo de Pago</strong>
									</td>
									<td>
										Tarjeta de Crédito
									</td>
								</tr>
								<tr>
									<td>
										<strong>COSTO TOTAL</strong>
									</td>
									<td>
										$154
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					<!--End step -->
				</div>
				<!--End col -->

				<aside class="col-lg-4">
					<div class="box_style_1">
						<h3 class="inner">Gracias!</h3>
						<p>
							Nihil inimicus ex nam, in ipsum dignissim duo. Tale principes interpretaris vim ei, has posidonium definitiones ut. Duis harum fuisset ut his, duo an dolor epicuri appareat.
						</p>
					</div>
					<div class="box_style_4">
                        <?php require 'telefono.php';?>
					</div>
				</aside>

			</div>
			<!--End row -->
		</div>
		<!--End container -->
	</main>
	<!-- End main -->

            <?php require 'footer.php';?>

</body>

</html>