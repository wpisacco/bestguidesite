<!DOCTYPE html>
<html lang="en">

<head>
      <?php require 'header.php';?>
</head>

<header>
    <div id="top_line">
        <?php //require 'header1.php';?>
    </div>
         <?php require 'header2.php';?>  
</header>

<body>
	<section class="parallax-window" data-parallax="scroll" data-image-src="img/slide_hero_4.jpg" data-natural-width="1400" data-natural-height="470">
		<div class="parallax-content-1">
			<div class="animated fadeInDown">
				<h1>Hola Ceci!</h1>
				<p>Ridiculus sociosqu cursus neque cursus curae ante scelerisque vehicula.</p>
			</div>
		</div>
	</section>
	<!-- End section -->

	<main>
		<div id="position">
			<div class="container">
			</div>
		</div>
		<!-- End Position -->

		<div class="margin_60 container">
			<div id="tabs" class="tabs">
				<nav>
					<ul>
                        <li><a href="#section-4" class="icon-profile"><span>Datos Personales</span></a></li>
                        <li><a href="#section-1" class="icon-profile"><span>Tus Cualidades</span></a></li>
                        <li><a href="#section-2" class="icon-profile"><span>Información para Guías</span></a></li>
                        <li><a href="#section-3" class="icon-settings"><span>Configuración</span></a></li>
					</ul>
				</nav>
				<div class="content">
					<section id="section-4">
						<div class="row">
							<div class="col-md-6">
								<h4>Tu perfil</h4>
								<ul id="profile_summary">
									<li>Usuario <span>clarita</span></li>
									<li>Nombre y Apellido <span>Clara Gomez</span></li>
                                    <li>Género <span>Chico</span></li>                                    
									<li>Fecha Nacimiento <span>13/04/1975</span></li>
									<li>Dirección <span>24 Rue de Rivoli, Paris, Francia</span></li>
                                    <li>Teléfono <span>+054 032 42366 / +054 11 65309188</span></li>
                                    <li>Email <span>wpisacco@gmail.com</span></li>                                    
								</ul>
							</div>
							<div class="col-md-6">
								<p>
								<img src="img/tourist_guide_pic.jpg" alt="Image" class="img-fluid styled profile_pic">
								</p>
							</div>
						</div>
						<!-- End row -->

						<div class="divider"></div>

						<div class="row">
							<div class="col-md-12">
								<h4>Editar Perfil</h4>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label>Nombres</label>
									<input class="form-control" name="first_name" id="first_name" type="text">
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label>Apellido</label>
									<input class="form-control" name="last_name" id="last_name" type="text">
								</div>
							</div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Como deseas ser llamado</label>
                                    <input class="form-control" name="alias" id="alias" type="text">
                                </div>
                            </div>                             
						</div>
						<!-- End row -->

						<div class="row">
							<div class="col-md-4">
								<div class="form-group">
									<label>Género</label>
									<Select class="form-control" name="genero" id="genero">
                                    <option value"2">Chico</option>
                                    <option value"3">Chica</option>
                                    <option value"4">LGBT</option>
                                    <option value"1">Sin Informar</option>
                                   </Select> 
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label>Fecha de Nacimiento <small>(dd/mm/yyyy)</small>
									</label>
                                    <input class="form-control booking_date" type="text" data-lang="en" data-large-mode="true" data-large-default="true" data-min-year="2017" data-max-year="2020" data-disabled-days="11/17/2017,12/17/2017">
								</div>
							</div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Nro Documento</label>
                                    <input class="form-control" name="nrodoc" id="nrodoc" type="text">
                                </div>
                            </div>                            
						</div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Teléfono Cel.</label>
                                    <input class="form-control" name="tele1" id="tele1" type="text">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Teléfono Fijo</label>
                                    <input class="form-control" name="tele2" id="tele2" type="text">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>mail</label>
                                    <input class="form-control" name="email" id="email" type="text">
                                </div>
                            </div>
                        </div>
						<!-- End row -->

						<hr>
						<div class="row">
							<div class="col-md-12">
								<h4>Editar Dirección</h4>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label>Calle/Altura</label>
									<input class="form-control" name="calle" id="calle" type="text">
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label>Ciudad</label>
									<input class="form-control" name="ciudad" id="ciudad" type="text">
								</div>
							</div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Estado/Pcia</label>
                                    <input class="form-control" name="estado" id="estado" type="text">
                                </div>
                            </div>                            
						</div>
						<!-- End row -->

						<div class="row">
							<div class="col-md-2">
								<div class="form-group">
									<label>Código Postal</label>
									<input class="form-control" name="email" id="email" type="text">
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label>Pais</label>
									<select id="country" class="form-control" name="country">
										<option value="">Select...</option>
                                        <option value="1">Argentina</option>
									</select>
								</div>
							</div>
						</div>
						<!-- End row -->

						<hr>
						<h4>Actualizar foto de Perfil</h4>
						<div class="form-inline upload_1">
							<div class="form-group">
								<input type="file" name="files[]" id="js-upload-files" multiple>
							</div>
							<button type="submit" class="btn_1 green" id="js-upload-submit">Subir Archivo</button>
						</div>
							<!-- Drop Zone -->
							<h5>O arrastrar el archivo sobre el recuadro</h5>
							<div class="upload-drop-zone" id="drop-zone">
								Arrastre los archivos aquí
							</div>
							<!-- Progress Bar -->
							<div class="progress">
								<div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;">
									<span class="sr-only">60% Complete</span>
								</div>
							</div>
							<!-- Upload Finished -->
							<div class="js-upload-finished">
								<h5>Archivos Procesados</h5>
								<div class="list-group">
									<a href="#" class="list-group-item list-group-item-success"><span class="badge alert-success pull-right">Exitoso</span>image-01.jpg</a>
								</div>
							</div>
							<!-- End Hidden on mobiles -->

							<hr>
							<button type="submit" class="btn_1 green">Actualizar Datos Personales</button>
					</section>


                    <section id="section-1">
                        <div class="row">
                            <div class="col-lg-3">
                                <h4>Idiomas</h4>
                                <table class="table table-striped options_cart">
                                    <tbody>
                                        <tr>
                                            <td style="width:10%">
                                                <img class="bandera" src="img/paises/Spain.png"/>
                                            </td>
                                            <td style="width:60%">
                                                Español
                                            </td>
                                            <td style="width:35%">
                                                <label class="switch-light switch-ios pull-right">
                                                    <input type="checkbox" name="option_1" id="option_1" checked value="">
                                                    <span>
                                                    <span>No</span>
                                                    <span>Si</span>
                                                    </span>
                                                    <a></a>
                                                </label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <img class="bandera" src="img/paises/United-Kingdom.png"/>
                                            </td>
                                            <td>
                                                Ingles
                                            </td>
                                            <td>
                                                <label class="switch-light switch-ios pull-right">
                                                    <input type="checkbox" name="option_2" id="option_2" value="">
                                                    <span>
                                                    <span>No</span>
                                                    <span>Si</span>
                                                    </span>
                                                    <a></a>
                                                </label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <img class="bandera" src="img/paises/Brazil.png"/>
                                            </td>
                                            <td>
                                                Portugues
                                            </td>
                                            <td>
                                                <label class="switch-light switch-ios pull-right">
                                                    <input type="checkbox" name="option_3" id="option_3" value="" >
                                                    <span>
                                                        <span>No</span>
                                                    <span>Si</span>
                                                    </span>
                                                    <a></a>
                                                </label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                               <img class="bandera" src="img/paises/France.png"/>
                                            </td>
                                            <td>
                                                Frances
                                            </td>
                                            <td>
                                                <label class="switch-light switch-ios pull-right">
                                                    <input type="checkbox" name="option_4" id="option_4" value="">
                                                    <span>
                                                        <span>No</span>
                                                    <span>Si</span>
                                                    </span>
                                                    <a></a>
                                                </label>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-lg-1">
                            </div>
                            <div class="col-lg-3">
                                <h4>Gustos</h4>
                                <table class="table table-striped options_cart">
                                    <tbody>
                                        <tr>
                                            <td style="width:10%">
                                                <i class="icon_set_1_icon-40"></i>
                                            </td>
                                            <td style="width:60%">
                                                Paseos en Bicicleta
                                            </td>
                                            <td style="width:35%">
                                                <label class="switch-light switch-ios pull-right">
                                                    <input type="checkbox" name="option_1" id="option_1" value="">
                                                    <span>
                                                    <span>No</span>
                                                    <span>Si</span>
                                                    </span>
                                                    <a></a>
                                                </label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="icon_set_1_icon-58"></i>
                                            </td>
                                            <td>
                                                Comidas
                                            </td>
                                            <td>
                                                <label class="switch-light switch-ios pull-right">
                                                    <input type="checkbox" name="option_2" id="option_2" value="">
                                                    <span>
                                                    <span>No</span>
                                                    <span>Si</span>
                                                    </span>
                                                    <a></a>
                                                </label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="icon_set_1_icon-30"></i>
                                            </td>
                                            <td>
                                                Caminatas
                                            </td>
                                            <td>
                                                <label class="switch-light switch-ios pull-right">
                                                    <input type="checkbox" name="option_3" id="option_3" value="" >
                                                    <span>
                                                        <span>No</span>
                                                    <span>Si</span>
                                                    </span>
                                                    <a></a>
                                                </label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="icon_set_1_icon-8"></i>
                                            </td>
                                            <td>
                                                Playa
                                            </td>
                                            <td>
                                                <label class="switch-light switch-ios pull-right">
                                                    <input type="checkbox" name="option_4" id="option_4" value="">
                                                    <span>
                                                        <span>No</span>
                                                    <span>Si</span>
                                                    </span>
                                                    <a></a>
                                                </label>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-lg-1">
                            </div>                            
                             <div class="col-lg-3">
                                 <h4>.</h4>
                                <table class="table table-striped options_cart">
                                    <tbody>
                                        <tr>
                                            <td style="width:10%">
                                                <i class="icon_set_1_icon-40"></i>
                                            </td>
                                            <td style="width:60%">
                                                Paseos en Bicicleta
                                            </td>
                                            <td style="width:35%">
                                                <label class="switch-light switch-ios pull-right">
                                                    <input type="checkbox" name="option_1" id="option_1" value="">
                                                    <span>
                                                    <span>No</span>
                                                    <span>Si</span>
                                                    </span>
                                                    <a></a>
                                                </label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="icon_set_1_icon-58"></i>
                                            </td>
                                            <td>
                                                Comidas
                                            </td>
                                            <td>
                                                <label class="switch-light switch-ios pull-right">
                                                    <input type="checkbox" name="option_2" id="option_2" value="">
                                                    <span>
                                                    <span>No</span>
                                                    <span>Si</span>
                                                    </span>
                                                    <a></a>
                                                </label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="icon_set_1_icon-30"></i>
                                            </td>
                                            <td>
                                                Caminatas
                                            </td>
                                            <td>
                                                <label class="switch-light switch-ios pull-right">
                                                    <input type="checkbox" name="option_3" id="option_3" value="" >
                                                    <span>
                                                        <span>No</span>
                                                    <span>Si</span>
                                                    </span>
                                                    <a></a>
                                                </label>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>   
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                 <button type="submit" class="btn_1 green">Actualizar Cualidades</button>
                            </div>                            
                        </div>                                
                    </section>


                    <section id="section-2">
                        <div class="row">
                            <div class="col-md-6 add_bottom_30">
                                <div class="form-group">
                                    <label>Profesión</label>
                                    <input class="form-control" name="profesion" id="profesion" type="text">
                                </div>
                             </div>
                             <div class="col-md-6 add_bottom_30">    
                                <div class="form-group">
                                    <label>Frase de presentación</label>
                                    <input class="form-control" name="frase" id="frase" type="text">
                                </div>
                            </div>
                            <div class="col-md-12 add_bottom_30">
                                <div class="form-group">
                                    <label>Cuentanos sobre ti y tu ciudad</label>
                                    <textarea class="form-control" name="hable" id="hable"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2 add_bottom_30">
                                <div class="form-group">
                                    <label>Precio/hora (US$)</label>
                                    <select class="form-control" name="precio" id="precio">
                                        <option selected value="5">5</option>
                                        <option value="10">10</option>
                                        <option value="15">15</option>
                                        <option value="20">20</option>
                                        <option value="30">30</option>                                        
                                    </select>

                                </div>
                             </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                 <button type="submit" class="btn_1 green">Actualizar Información</button>
                            </div>                            
                        </div>                                
                    </section>

                    <section id="section-3">
                        <div class="row">
                            <div class="col-md-6 add_bottom_30">
                                <h4>Cambiar Contraseña</h4>
                                <div class="form-group">
                                    <label>Vieja contraseña</label>
                                    <input class="form-control" name="old_password" id="old_password" type="password">
                                </div>
                                <div class="form-group">
                                    <label>Nueva contraseña</label>
                                    <input class="form-control" name="new_password" id="new_password" type="password">
                                </div>
                                <div class="form-group">
                                    <label>Confirmar nueva contraseña</label>
                                    <input class="form-control" name="confirm_new_password" id="confirm_new_password" type="password">
                                </div>
                                <button type="submit" class="btn_1 green">Actualizar Contraseña</button>
                            </div>
                            <div class="col-md-6 add_bottom_30">
                                <h4>Cambiar email</h4>
                                <div class="form-group">
                                    <label>Viejo email</label>
                                    <input class="form-control" name="old_password" id="old_password" type="password">
                                </div>
                                <div class="form-group">
                                    <label>Nuevo email</label>
                                    <input class="form-control" name="new_password" id="new_password" type="password">
                                </div>
                                <div class="form-group">
                                    <label>Confirmar nuevo email</label>
                                    <input class="form-control" name="confirm_new_password" id="confirm_new_password" type="password">
                                </div>
                                <button type="submit" class="btn_1 green">Actualizar Email</button>
                            </div>
                        </div>
                            
                    </section>


					</div>
					<!-- End content -->
				</div>
				<!-- End tabs -->
			</div>
			<!-- end container -->
	</main>
            <?php require 'footer.php';?>
</body>

</html>

    <!-- Specific scripts -->
    <script src="js/tabs.js"></script>
    <script>
        new CBPFWTabs(document.getElementById('tabs'));
    </script>
    <script>
        $('.wishlist_close_admin').on('click', function (c) {
            $(this).parent().parent().parent().fadeOut('slow', function (c) {});
        });
    </script>
