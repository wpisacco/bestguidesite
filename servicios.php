<!DOCTYPE html>
<html lang="en">

<head>
      <?php require 'header.php';?>
</head>

<header>
    <div id="top_line">
        <?php require 'header1.php';?>
    </div>
         <?php require 'header2.php';?>  
</header>

	<section class="parallax-window" data-parallax="scroll" data-image-src="img/sercicios_top.jpg" data-natural-width="1400" data-natural-height="470">
		<div class="parallax-content-1">
			<div class="animated fadeInDown">
				<h1>Servicios</h1>
				<p>Ridiculus sociosqu cursus neque cursus curae ante scelerisque vehicula.</p>
			</div>
		</div>
	</section>
	<!-- End section -->

	<main>
		<div id="position">
			<div class="container">
			</div>
		</div>
		<!-- Position -->

		<div class="container margin_60">

			<div class="row">
				<aside class="col-lg-3">

					<div id="filters_col">
						<a data-toggle="collapse" href="#collapseFilters" aria-expanded="false" aria-controls="collapseFilters" id="filters_col_bt"><i class="icon_set_1_icon-65"></i>Filtros</a>
						<div class="collapse shwo" id="collapseFilters">
							<div class="filter_type">
								<h6>Precios</h6>
								<input type="text" id="range" name="range" value="">
							</div>
							<div class="filter_type">
								<h6>Elegido</h6>
								<ul>
									<li>
										<label>
											<input type="checkbox"><span class="rating">
											<i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile voted"></i>
											</span>
										</label>
									</li>
									<li>
										<label>
											<input type="checkbox"><span class="rating">
											<i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile"></i>
											</span>
										</label>
									</li>
									<li>
										<label>
											<input type="checkbox"><span class="rating">
											<i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile"></i><i class="icon-smile"></i>
											</span>
										</label>
									</li>
									<li>
										<label>
											<input type="checkbox"><span class="rating">
											<i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile"></i><i class="icon-smile"></i><i class="icon-smile"></i>
											</span>
										</label>
									</li>
									<li>
										<label>
											<input type="checkbox"><span class="rating">
											<i class="icon-smile voted"></i><i class="icon-smile"></i><i class="icon-smile"></i><i class="icon-smile"></i><i class="icon-smile"></i>
											</span>
										</label>
									</li>
								</ul>
							</div>
							<div class="filter_type">
								<h6>Facilidad</h6>
								<ul>
									<li>
										<label>
											<input type="checkbox">Mascotas
										</label>
									</li>
									<li>
										<label>
											<input type="checkbox">Groupos
										</label>
									</li>
									<li>
										<label>
											<input type="checkbox">Tours
										</label>
									</li>
									<li>
										<label>
											<input type="checkbox">Acceso a Discapacitados
										</label>
									</li>
								</ul>
							</div>
						</div>
						<!--End collapse -->
					</div>
					<!--End filters col-->
                    <div class="box_style_2">
                        <?php require 'telefono.php';?>
                    </div>
				</aside>
				<!--End aside -->
				
				<div class="col-lg-9">

					<div id="tools">
						<div class="row">
							<div class="col-md-3 col-sm-4 col-6">
								<div class="styled-select-filters">
									<select name="sort_price" id="sort_price">
										<option value="" selected>Ordenar por Precio</option>
										<option value="lower">Precio mas bajo</option>
										<option value="higher">Precio mas Alto</option>
									</select>
								</div>
							</div>
							<div class="col-md-3 col-sm-4 col-6">
								<div class="styled-select-filters">
									<select name="sort_rating" id="sort_rating">
										<option value="" selected>Ordenar por elegido</option>
										<option value="lower">Menos elegido</option>
										<option value="higher">Mas elegido</option>
									</select>
								</div>
							</div>
							<div class="col-md-6 col-sm-4 d-none d-sm-block text-right">
								<a href="all_transfer_grid.html" class="bt_filters"><i class="icon-th"></i></a> <a href="#" class="bt_filters"><i class=" icon-list"></i></a>
							</div>

						</div>
					</div>
					<!--/tools -->

					<div class="strip_all_tour_list wow fadeIn" data-wow-delay="0.1s">
						<div class="row">
							<div class="col-lg-4 col-md-4">
								<div class="ribbon_3 popular"><span>Popular</span>
								</div>
								<div class="wishlist">
									<a class="tooltip_flip tooltip-effect-1" href="javascript:void(0);">+<span class="tooltip-content-flip"><span class="tooltip-back">Agregar a Deseos</span></span></a>
								</div>
								<div class="img_list">
									<a href="single_transfer.html"><img src="img/bicicleta.jpg" alt="Image">
										<div class="short_info"></div>
									</a>
								</div>
							</div>
							<div class="col-lg-6 col-md-6">
								<div class="tour_list_desc">
									<div class="rating"><i class="icon-smile voted"></i><i class="icon-smile  voted"></i><i class="icon-smile  voted"></i><i class="icon-smile  voted"></i><i class="icon-smile"></i><small>(75)</small>
									</div>
									<h3><strong>Alquiler de Bicicletas</strong> Personal</h3>
									<p>Lorem ipsum dolor sit amet, quem convenire interesset ut vix, ad dicat sanctus detracto vis. Eos modus dolorum ex...</p>
									<ul class="add_info">
										<li>
											<div class="tooltip_styled tooltip-effect-4">
												<span class="tooltip-item"><i class="icon_set_1_icon-70"></i></span>
												<div class="tooltip-content">
													<h4>Passengers</h4> Up to 6 passengers.
												</div>
											</div>
										</li>
										<li>
											<div class="tooltip_styled tooltip-effect-4">
												<span class="tooltip-item"><i class="icon_set_1_icon-6"></i></span>
												<div class="tooltip-content">
													<h4>Pick up</h4> Hotel pick up or different place with an extra cost.
												</div>
											</div>
										</li>
										<li>
											<div class="tooltip_styled tooltip-effect-4">
												<span class="tooltip-item"><i class="icon_set_1_icon-13"></i></span>
												<div class="tooltip-content">
													<h4>Accessibility</h4> On request accessibility available.
												</div>
											</div>
										</li>
										<li>
											<div class="tooltip_styled tooltip-effect-4">
												<span class="tooltip-item"><i class="icon_set_1_icon-22"></i></span>
												<div class="tooltip-content">
													<h4>Pet allowed</h4> On request pet allowed.
												</div>
											</div>
										</li>
										<li>
											<div class="tooltip_styled tooltip-effect-4">
												<span class="tooltip-item"><i class="icon_set_1_icon-33"></i></span>
												<div class="tooltip-content">
													<h4>Baggage</h4> Large baggage drop available.
												</div>
											</div>
										</li>
									</ul>
								</div>
							</div>
							<div class="col-lg-2 col-md-2">
								<div class="price_list">
									<div><sup>US$</sup>29*<span class="normal_price_list">$59</span><small>*Desde/Por persona</small>
										<p><a href="single_transfer.html" class="btn_1">Detalles</a>
										</p>
									</div>

								</div>
							</div>
						</div>
					</div>
					<!--End strip -->

					<div class="strip_all_tour_list wow fadeIn" data-wow-delay="0.1s">
						<div class="row">
							<div class="col-lg-4 col-md-4">
								<div class="ribbon_3 popular"><span>Popular</span>
								</div>
								<div class="wishlist">
									<a class="tooltip_flip tooltip-effect-1" href="javascript:void(0);">+<span class="tooltip-content-flip"><span class="tooltip-back">Agregar a Deseos</span></span></a>
								</div>
								<div class="img_list">
									<a href="single_transfer.html"><img src="img/bus.jpg" alt="Image">
										<div class="short_info"></div>
									</a>
								</div>
							</div>
							<div class="col-lg-6 col-md-6">
								<div class="tour_list_desc">
									<div class="rating"><i class="icon-smile voted"></i><i class="icon-smile  voted"></i><i class="icon-smile  voted"></i><i class="icon-smile  voted"></i><i class="icon-smile"></i><small>(75)</small>
									</div>
									<h3><strong>Bus Turístico</strong> Grupal</h3>
									<p>Lorem ipsum dolor sit amet, quem convenire interesset ut vix, ad dicat sanctus detracto vis. Eos modus dolorum ex...</p>
									<ul class="add_info">
										<li>
											<div class="tooltip_styled tooltip-effect-4">
												<span class="tooltip-item"><i class="icon_set_1_icon-70"></i></span>
												<div class="tooltip-content">
													<h4>Pasajeros</h4> Mas de 3 pasajeros.
												</div>
											</div>
										</li>
										<li>
											<div class="tooltip_styled tooltip-effect-4">
												<span class="tooltip-item"><i class="icon_set_1_icon-6"></i></span>
												<div class="tooltip-content">
													<h4>Pick up</h4> Hotel pick up or different place with an extra cost.
												</div>
											</div>
										</li>
										<li>
											<div class="tooltip_styled tooltip-effect-4">
												<span class="tooltip-item"><i class="icon_set_1_icon-13"></i></span>
												<div class="tooltip-content">
													<h4>Accessibility</h4> On request accessibility available.
												</div>
											</div>
										</li>
										<li>
											<div class="tooltip_styled tooltip-effect-4">
												<span class="tooltip-item"><i class="icon_set_1_icon-22"></i></span>
												<div class="tooltip-content">
													<h4>Pet allowed</h4> On request pet allowed.
												</div>
											</div>
										</li>
										<li>
											<div class="tooltip_styled tooltip-effect-4">
												<span class="tooltip-item"><i class="icon_set_1_icon-33"></i></span>
												<div class="tooltip-content">
													<h4>Baggage</h4> Large baggage drop available.
												</div>
											</div>
										</li>
									</ul>
								</div>
							</div>
							<div class="col-lg-2 col-md-2">
								<div class="price_list">
									<div><sup>$</sup>59*<span class="normal_price_list">$99</span><small>*Desde/Por persona</small>
										<p><a href="single_transfer.html" class="btn_1">Detalles</a>
										</p>
									</div>

								</div>
							</div>
						</div>
					</div>
					<!--End strip -->

					<div class="strip_all_tour_list wow fadeIn" data-wow-delay="0.1s">
						<div class="row">
							<div class="col-lg-4 col-md-4">
								<div class="ribbon_3"><span>Mas Elegido</span>
								</div>
								<div class="wishlist">
									<a class="tooltip_flip tooltip-effect-1" href="javascript:void(0);">+<span class="tooltip-content-flip"><span class="tooltip-back">Agregar a Deseos</span></span></a>
								</div>
								<div class="img_list">
									<a href="single_transfer.html"><img src="img/transfer.jpg" alt="Image">
										<div class="short_info"></div>
									</a>
								</div>
							</div>
							<div class="col-lg-6 col-md-6">
								<div class="tour_list_desc">
									<div class="rating"><i class="icon-smile voted"></i><i class="icon-smile  voted"></i><i class="icon-smile  voted"></i><i class="icon-smile  voted"></i><i class="icon-smile"></i><small>(75)</small>
									</div>
									<h3><strong>Transfer Aeropuerto</strong> Privado</h3>
									<p>Lorem ipsum dolor sit amet, quem convenire interesset ut vix, ad dicat sanctus detracto vis. Eos modus dolorum ex...</p>
									<ul class="add_info">
										<li>
											<div class="tooltip_styled tooltip-effect-4">
												<span class="tooltip-item"><i class="icon_set_1_icon-70"></i></span>
												<div class="tooltip-content">
													<h4>Pasajeros</h4> Hasta 3 pasajeros.
												</div>
											</div>
										</li>
										<li>
											<div class="tooltip_styled tooltip-effect-4">
												<span class="tooltip-item"><i class="icon_set_1_icon-6"></i></span>
												<div class="tooltip-content">
													<h4>Pick up</h4> Hotel pick up or different place with an extra cost.
												</div>
											</div>
										</li>
										<li>
											<div class="tooltip_styled tooltip-effect-4">
												<span class="tooltip-item"><i class="icon_set_1_icon-13"></i></span>
												<div class="tooltip-content">
													<h4>Accessibility</h4> On request accessibility available.
												</div>
											</div>
										</li>
										<li>
											<div class="tooltip_styled tooltip-effect-4">
												<span class="tooltip-item"><i class="icon_set_1_icon-22"></i></span>
												<div class="tooltip-content">
													<h4>Pet allowed</h4> On request pet allowed.
												</div>
											</div>
										</li>
										<li>
											<div class="tooltip_styled tooltip-effect-4">
												<span class="tooltip-item"><i class="icon_set_1_icon-33"></i></span>
												<div class="tooltip-content">
													<h4>Baggage</h4> Large baggage drop available.
												</div>
											</div>
										</li>
									</ul>
								</div>
							</div>
							<div class="col-lg-2 col-md-2">
								<div class="price_list">
									<div><sup>$</sup>39*<span class="normal_price_list">$99</span><small>*Desde/Cada 10 kms</small>
										<p><a href="single_transfer.html" class="btn_1">Detalles</a>
										</p>
									</div>

								</div>
							</div>
						</div>
					</div>
					<!--End strip -->
					<hr>

					<nav aria-label="Page navigation">
						<ul class="pagination justify-content-center">
							<li class="page-item">
								<a class="page-link" href="#" aria-label="Previous">
									<span aria-hidden="true">&laquo;</span>
									<span class="sr-only">Previo</span>
								</a>
							</li>
							<li class="page-item active"><span class="page-link">1<span class="sr-only">(actual)</span></span>
							</li>
							<li class="page-item"><a class="page-link" href="#">2</a></li>
							<li class="page-item"><a class="page-link" href="#">3</a></li>
							<li class="page-item">
								<a class="page-link" href="#" aria-label="Next">
									<span aria-hidden="true">&raquo;</span>
									<span class="sr-only">Próximo</span>
								</a>
							</li>
						</ul>
					</nav>
					<!-- end pagination-->
				</div>
				<!-- End col lg-9 -->
			</div>
			<!-- End row -->
		</div>
		<!-- End container -->
	</main>
	<!-- End main -->

    <!-- End main -->
            <?php require 'footer.php';?>

</body>

</html>
	<script>
		$('input').iCheck({
		   checkboxClass: 'icheckbox_square-grey',
		   radioClass: 'iradio_square-grey'
		 });
	</script>
