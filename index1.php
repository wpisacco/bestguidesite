<!DOCTYPE html>
<html lang="sp">

<head>
            <?php require 'header.php';?>
</head>

    <header>
        <div id="top_line">

            <?php require 'header1.php';?>

        </div><!-- End top line-->
             <?php require 'header2.php';?>       

    </header><!-- End Header -->
<body>
	<section id="hero">
		<div class="intro_title">
			<h3 class="animated fadeInDown">Se parte de cualquier lugar</h3>
			<p class="animated fadeInDown">Habla tu idioma / Descubre lugares / Disfruta la compañía</p>

		</div>

		<div id="search_bar_container">
			<div class="container">
				<div class="search_bar">
					<span class="nav-facade-active" id="nav-search-in">
								<span id="nav-search-in-content" style="">Guia Local</span>
					<span class="nav-down-arrow nav-sprite"></span>
					<select title="Search in" class="searchSelect" id="searchDropdownBox" name="tours_category">
                                    <option value="Guia"  title="Guia">Guia Local</option> 
									<option value="Bicicletas"  title="Alquiler Bicicletas">Alquiler de Bicicletas</option>
									<option value="Otros Servicios" title="Otros Servicio">Otros Servicios</option>
								</select>
					</span>
					<div class="nav-searchfield-outer">
						<input type="text" autocomplete="off" name="field-keywords" placeholder="Elije tu destino ...." id="twotabsearchtextbox">
					</div>
					<div class="nav-submit-button">
						<input type="submit" title="Cerca" class="nav-submit-input" value="Search">
					</div>
				</div>
				<!-- End search bar-->
			</div>
		</div>
		<!-- /search_bar-->
	</section>
	<!-- End hero -->

	<main>
	<div class="container margin_60">
    
        <div class="main_title">
            <h2>Nuestro <span>Mejor</span> Staff</h2>
            <p>Conoce a quienes han sido reconocidos por nuestros clientes.</p>
        </div>
        
        <div class="row">
        
            <div class="col-lg-4 col-md-6 wow zoomIn" data-wow-delay="0.1s">
                <div class="tour_container">
					<div class="ribbon_3 popular"><span>Popular</span></div>
                    <div class="img_container">
                        <a href="perfil.php">
                        <img src="img/tour_box_1.jpg" width="800" height="533" class="img-fluid" alt="Image">
                        <div class="short_info">
                            <img class="bandera" src="img/paises/Argentina.png"/>Argentina<span class="price"><sup>$</sup>10</span>
                        </div>
                        </a>
                    </div>
                    <div class="tour_title">
                        <h3><strong>Ceci</strong> (Buenos Aires)</h3>
                        <div class="rating">
                            <i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile"></i><small>(13)</small>
                        </div><!-- end rating -->
                        <div class="wishlist">
                            <a class="tooltip_flip tooltip-effect-1" href="javascript:void(0);">+<span class="tooltip-content-flip"><span class="tooltip-back">Agregar a Deseos</span></span></a>
                        </div><!-- End wish list-->
                    </div>
                </div><!-- End box tour -->
            </div><!-- End col -->
            
            <div class="col-lg-4 col-md-6 wow zoomIn" data-wow-delay="0.2s">
                <div class="tour_container">
					<div class="ribbon_3 popular"><span>Popular</span></div>
                    <div class="img_container">
                        <a href="perfil.php">
                        <img src="img/tour_box_2.jpg" width="800" height="533" class="img-fluid" alt="Image">
                        <div class="badge_save">Descuento<strong>20%</strong></div>
                        <div class="short_info">
                            <img class="bandera" src="img/paises/Brazil.png"/>Brasil<span class="price"><sup>$</sup>12</span>
                        </div>
                        </a>
                    </div>
                    <div class="tour_title">
                        <h3><strong>Orlando</strong> (San Pablo)</h3>
                        <div class="rating">
                            <i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile"></i><small>(12)</small>
                        </div><!-- end rating -->
                        <div class="wishlist">
                            <a class="tooltip_flip tooltip-effect-1" href="javascript:void(0);">+<span class="tooltip-content-flip"><span class="tooltip-back">Agregar a Deseos</span></span></a>
                        </div><!-- End wish list-->
                    </div>
                </div><!-- End box tour -->
            </div><!-- End col -->
            
            <div class="col-lg-4 col-md-6 wow zoomIn" data-wow-delay="0.3s">
                <div class="tour_container">
					<div class="ribbon_3 popular"><span>Popular</span></div>
                    <div class="img_container">
                        <a href="perfil.php">
                        <img src="img/tour_box_3.jpg" width="800" height="533" class="img-fluid" alt="Image">
                        <div class="short_info">
                            <img class="bandera" src="img/paises/Argentina.png"/>Argentina<span class="price"><sup>$</sup>8</span>
                        </div>
                        </a>
                    </div>
                    <div class="tour_title">
                        <h3><strong>Karina</strong> (Bariloche)</h3>
                        <div class="rating">
                            <i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile"></i><small>(11)</small>
                        </div><!-- end rating -->
                        <div class="wishlist">
                            <a class="tooltip_flip tooltip-effect-1" href="javascript:void(0);">+<span class="tooltip-content-flip"><span class="tooltip-back">Agregar a Deseos</span></span></a>
                        </div><!-- End wish list-->
                    </div>
                </div><!-- End box tour -->
            </div><!-- End col -->
            
            <div class="col-lg-4 col-md-6 wow zoomIn" data-wow-delay="0.4s">
                <div class="tour_container">
					<div class="ribbon_3"><span>+ Votados</span></div>
                    <div class="img_container">
                        <a href="perfil.php">
                        <img src="img/tour_box_4.jpg" width="800" height="533" class="img-fluid" alt="Image">
                        <div class="short_info">
                            <img class="bandera" src="img/paises/Spain.png"/>España<span class="price"><sup>$</sup>20</span>
                        </div>
                        </a>
                    </div>
                    <div class="tour_title">
                        <h3><strong>Samantha</strong> (Barcelona)</h3>
                        <div class="rating">
                            <i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile "></i><i class="icon-smile"></i><small>(10)</small>
                        </div><!-- end rating -->
                        <div class="wishlist">
                            <a class="tooltip_flip tooltip-effect-1" href="javascript:void(0);">+<span class="tooltip-content-flip"><span class="tooltip-back">Agregar a Deseos</span></span></a>
                        </div><!-- End wish list-->
                    </div>
                </div><!-- End box tour -->
            </div><!-- End col -->
            
            <div class="col-lg-4 col-md-6 wow zoomIn" data-wow-delay="0.5s">
                <div class="tour_container">
					<div class="ribbon_3"><span>+ Votados</span></div>
                    <div class="img_container">
                        <a href="perfil.php">
                        <img src="img/tour_box_14.jpg" width="800" height="533" class="img-fluid" alt="Image">
                        <div class="short_info">
                            <img class="bandera" src="img/paises/France.png"/>Francia<span class="price"><sup>$</sup>15</span>
                        </div>
                        </a>
                    </div>
                    <div class="tour_title">
                        <h3><strong>Poul</strong> (Paris)</h3>
                        <div class="rating">
                            <i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile "></i><i class="icon-smile"></i><small>(8)</small>
                        </div><!-- end rating -->
                        <div class="wishlist">
                            <a class="tooltip_flip tooltip-effect-1" href="javascript:void(0);">+<span class="tooltip-content-flip"><span class="tooltip-back">Agregar a Deseos</span></span></a>
                        </div><!-- End wish list-->
                    </div>
                </div><!-- End box tour -->
            </div><!-- End col -->
            
            <div class="col-lg-4 col-md-6 wow zoomIn" data-wow-delay="0.6s">
                <div class="tour_container">
					<div class="ribbon_3"><span>+ Votados</span></div>
                    <div class="img_container">
                        <a href="perfil.php">
                        <img src="img/tour_box_5.jpg" width="800" height="533" class="img-fluid" alt="Image">
                        <div class="short_info">
                            <img class="bandera" src="img/paises/Brazil.png"/>Brasil<span class="price"><sup>$</sup>15</span>
                        </div>
                        </a>
                    </div>
                    <div class="tour_title">
                        <h3><strong>Bernard</strong> (Rio)</h3>
                        <div class="rating">
                            <i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile "></i><i class="icon-smile"></i><small>(10)</small>
                        </div><!-- end rating -->
                        <div class="wishlist">
                            <a class="tooltip_flip tooltip-effect-1" href="javascript:void(0);">+<span class="tooltip-content-flip"><span class="tooltip-back">Agregar a Deseos</span></span></a>
                        </div><!-- End wish list-->
                    </div>
                </div><!-- End box tour -->
            </div><!-- End col -->
            
            <div class="col-lg-4 col-md-6 wow zoomIn" data-wow-delay="0.7s">
                <div class="tour_container">
					<div class="ribbon_3"><span>+ Votados</span></div>
                    <div class="img_container">
                        <a href="perfil.php">
                        <img src="img/tour_box_8.jpg" width="800" height="533" class="img-fluid" alt="Image">
                        <div class="short_info">
                            <img class="bandera" src="img/paises/Argentina.png"/>Argentina<span class="price"><sup>$</sup>10</span>
                        </div>
                        </a>
                    </div>
                    <div class="tour_title">
                        <h3><strong>Alejandra</strong> (Mar del Plata)</h3>
                        <div class="rating">
                            <i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile "></i><i class="icon-smile"></i><small>(6)</small>
                        </div><!-- end rating -->
                        <div class="wishlist">
                            <a class="tooltip_flip tooltip-effect-1" href="javascript:void(0);">+<span class="tooltip-content-flip"><span class="tooltip-back">Agregar a Deseos</span></span></a>
                        </div><!-- End wish list-->
                    </div>
                </div><!-- End box tour -->
            </div><!-- End col -->
            
            <div class="col-lg-4 col-md-6 wow zoomIn" data-wow-delay="0.8s">
                <div class="tour_container">
					<div class="ribbon_3"><span>+ Votados</span></div>
                    <div class="img_container">
                        <a href="perfil.php">
                        <img src="img/tour_box_9.jpg" width="800" height="533" class="img-fluid" alt="Image">
                        <div class="short_info">
                            <img class="bandera" src="img/paises/Chile.png"/>Chile<span class="price"><sup>$</sup>12</span>
                        </div>
                        </a>
                    </div>
                    <div class="tour_title">
                        <h3><strong>Estefan</strong> (Santiago)</h3>
                        <div class="rating">
                            <i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile"></i><small>(6)</small>
                        </div><!-- end rating -->
                        <div class="wishlist">
                            <a class="tooltip_flip tooltip-effect-1" href="javascript:void(0);">+<span class="tooltip-content-flip"><span class="tooltip-back">Agregar a Deseos</span></span></a>
                        </div><!-- End wish list-->
                    </div>
                </div><!-- End box tour -->
            </div><!-- End col -->
            
            <div class="col-lg-4 col-md-6 wow zoomIn" data-wow-delay="0.9s">
                <div class="tour_container">
					<div class="ribbon_3"><span>+ Votados</span></div>
                    <div class="img_container">
                        <a href="perfil.php">
                        <img src="img/tour_box_12.jpg" width="800" height="533" class="img-fluid" alt="Image">
                        <div class="short_info">
                            <img class="bandera" src="img/paises/Argentina.png"/>Argentina<span class="price"><sup>$</sup>15</span>
                        </div>
                        </a>
                    </div>
                    <div class="tour_title">
                        <h3><strong>Mia</strong> (Buenos Aires)</h3>
                        <div class="rating">
                            <i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile"></i><small>(5)</small>
                        </div><!-- end rating -->
                        <div class="wishlist">
                            <a class="tooltip_flip tooltip-effect-1" href="javascript:void(0);">+<span class="tooltip-content-flip"><span class="tooltip-back">Agregar a Deseos</span></span></a>
                        </div><!-- End wish list-->
                    </div>
                </div><!-- End box tour -->
            </div><!-- End col -->
            
        </div><!-- End row -->
		
        <p class="text-center nopadding">
            <a href="#" class="btn_1 medium"><i class="icon-eye-7"></i>Ver mas.. </a>
        </p>
    </div><!-- End container -->
    
    <div class="white_bg">
			<div class="container margin_60">
				<div class="main_title">
					<h2>Nuestros <span>Mejores</span> Servicios</h2>
					<p>
						No solamente lo acompañamos durante en su recorrido sinó que le facilitamos su estadía
					</p>
				</div>
				<div class="row add_bottom_45">
					<div class="col-lg-4 other_tours">
						<ul>
							<li><a href="#"><i class="icon_set_1_icon-3"></i>Bicicletas<span class="other_tours_price">$42</span></a>
							</li>
							<li><a href="#"><i class="icon_set_1_icon-30"></i>Transporte Público<span class="other_tours_price">$35</span></a>
							</li>
						</ul>
					</div>
					<div class="col-lg-4 other_tours">
						<ul>
                            <li><a href="#"><i class="icon_set_1_icon-30"></i>Caminatas<span class="other_tours_price">$20</span></a>
                            </li>
                            <li><a href="#"><i class="icon_set_1_icon-44"></i>WIFI<span class="other_tours_price">$20</span></a>
                            </li>                            
						</ul>
					</div>
                    <div class="col-lg-4 other_tours">
                        <ul>
                            <li><a href="#"><i class="icon_set_1_icon-50"></i>Paseo de Compras<span class="other_tours_price">$20</span></a>
                            </li>
                        </ul>
                    </div>
				</div>
				<!-- End row -->

				<div class="banner colored">
					<h4>Transporte Aeropuerto <span>desde U$S 50</span></h4>
					<p>
						El precio es aproximado, consulte disponibilidad y precios locales para su país.
					</p>
					<a href="#" class="btn_1 white">Ver mas</a>
				</div>
				
				<div class="row">
					<div class="col-lg-3 col-md-6 text-center">
						<p>
							<a href="#"><img src="img/bus.jpg" alt="Pic" class="img-fluid"></a>
						</p>
						<h4><span>Bus Turístico</span> booking</h4>
						<p>
							Lorem ipsum dolor sit amet, vix erat audiam ei. Cum doctus civibus efficiantur in. Nec id tempor imperdiet deterruisset, doctus volumus explicari qui ex.
						</p>
					</div>
					<div class="col-lg-3 col-md-6 text-center">
						<p>
							<a href="#"><img src="img/transfer.jpg" alt="Pic" class="img-fluid"></a>
						</p>
						<h4><span>Transfer</span> booking</h4>
						<p>
							Lorem ipsum dolor sit amet, vix erat audiam ei. Cum doctus civibus efficiantur in. Nec id tempor imperdiet deterruisset, doctus volumus explicari qui ex.
						</p>
					</div>
					<div class="col-lg-3 col-md-6 text-center">
						<p>
							<a href="#"><img src="img/guide.jpg" alt="Pic" class="img-fluid"></a>
						</p>
						<h4><span>Guía de Tuirismo</span> booking</h4>
						<p>
							Lorem ipsum dolor sit amet, vix erat audiam ei. Cum doctus civibus efficiantur in. Nec id tempor imperdiet deterruisset, doctus volumus explicari qui ex.
						</p>
					</div>
					<div class="col-lg-3 col-md-6 text-center">
						<p>
							<a href="#"><img src="img/hotel.jpg" alt="Pic" class="img-fluid"></a>
						</p>
						<h4><span>Hoteles</span> booking</h4>
						<p>
							Lorem ipsum dolor sit amet, vix erat audiam ei. Cum doctus civibus efficiantur in. Nec id tempor imperdiet deterruisset, doctus volumus explicari qui ex.
						</p>
					</div>
				</div>
				<!-- End row -->
				
			</div>
			<!-- End container -->
		</div>
		<!-- End white_bg -->

		<section class="promo_full">
			<div class="promo_full_wp magnific">
				<div>
					<h3>PERTENECE A CUALQUIER LUGAR</h3>
					<p>
						Lorem ipsum dolor sit amet, vix erat audiam ei. Cum doctus civibus efficiantur in. Nec id tempor imperdiet deterruisset, doctus volumus explicari qui ex.
					</p>
					<a href="https://www.youtube.com/watch?v=Zz5cu72Gv5Y" class="video"><i class="icon-play-circled2-1"></i></a>
				</div>
			</div>
		</section>
		<!-- End section -->

		<div class="container margin_60">

			<div class="main_title">
				<h2>Algunas <span>Buenas</span> Razones</h2>
				<p>
					Porqué somos la empresa de mayor confiabilidad en el mercado.
				</p>
			</div>

			<div class="row">

				<div class="col-lg-4 wow zoomIn" data-wow-delay="0.2s">
					<div class="feature_home">
						<i class="icon_set_1_icon-41"></i>
						<h3><span>+20</span> Ciudades</h3>
						<p>
							Estamos en gran parte de América y Europa, su decisión de ser parte puede sumar un lugar mas dentro de nustra gran familia.
						</p>
						<a href="about.html" class="btn_1 outline">Leer mas</a>
					</div>
				</div>

				<div class="col-lg-4 wow zoomIn" data-wow-delay="0.4s">
					<div class="feature_home">
						<i class="icon_set_1_icon-30"></i>
						<h3><span>+1000</span> Guías Registrados</h3>
						<p>
							Son muchos en todo el mundo los que confiaron, si usted conoce su territorio, tiene buena onda y piensa que puede ser parte de nosotros, hagalo ahora sin costo.
						</p>
						<a href="#" class="btn_1 outline">Leer mas</a>
					</div>
				</div>

				<div class="col-lg-4 wow zoomIn" data-wow-delay="0.6s">
					<div class="feature_home">
						<i class="icon_set_1_icon-57"></i>
						<h3><span>Soporte </span> H24</h3>
						<p>
							Nuestro personal lo asistirá en cualqueir momento del día de la manera mas cordial para que usted solo piense en disfrutar de su estancia.
						</p>
						<a href="about.html" class="btn_1 outline">Leer mas</a>
					</div>
				</div>

			</div>
			<!--End row -->

			<hr>

			<div class="row">
				<div class="col-md-6">
					<img src="img/laptop.png" alt="Laptop" class="img-fluid laptop">
				</div>
				<div class="col-md-6">
					<h3><span>Contacte a un Guía</span> con BESTGUIDE</h3>
					<p>
						En solo tres pasos sencillos usted se habrá asegurado que su estadía sea de lo mas placentera, como jamas ha soñado.
					</p>
					<ul class="list_order">
						<li><span>1</span>Elija su cuidad de destino</li>
						<li><span>2</span>Seleccione el guía y fecha de viaje</li>
						<li><span>3</span>Relice la Reserva</li>
					</ul>
					<a href="#" class="btn_1">Hagalo Ahora</a>
				</div>
			</div>
			<!-- End row -->
        
    	</div>
		<!-- End container -->
    </main>
	<!-- End main -->
	
            <?php require 'footer.php';?>
	

  </body>
</html>