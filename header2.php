<div class="container">
    <div class="row">
        <div class="col-3">
            <div id="logo_home">
            	<h1><a href="index.php" title="BestGuide.com.ar">BestGuide.com.ar</a></h1>
            </div>
        </div>
        <nav class="col-9">
            <a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a>
            <div class="main-menu">
                <div id="header_menu">
                    <img src="img/logo_sticky.png" width="160" height="34" alt="City tours" data-retina="true">
                </div>
                <a href="#" class="open_close" id="close_in"><i class="icon_set_1_icon-77"></i></a>
                <ul>
                    <li class=""><a href="index1.php" class="">Inicio <i class=""></i></a></li>
                    <li class=""><a href="servicios.php" class="">Servicios<i class=""></i></a></li>
                    <li class=""><a href="javascript:void(0);" class="">Se un Guía <i class=""></i></a></li>
                    <li class=""><a href="faq.php" class="">FAQ <i class=""></i></a></li>
                    <li class=""><a href="contact_us.php" class="">Contacto <i class=""></i></a></li>
                    <li class=""><a href="configuration.php" class="">Mi Perfil <i class=""></i></a></li>
                </ul>
            </div><!-- End main-menu -->
            <ul id="top_tools">
                <li>
                    <a href="javascript:void(0);" class="search-overlay-menu-btn"><i class="icon_search"></i></a>
                </li>
                <li>
                    <div class="dropdown dropdown-cart">
                        <a href="#" data-toggle="dropdown" class="cart_bt"><i class="icon_bag_alt"></i><strong>2</strong></a>
                        <ul class="dropdown-menu" id="cart_items">
                            <li>
                                <div class="image"><img src="img/thumb_cart_1.jpg" alt="image"></div>
                                <strong><a href="#">Ceci</a>1x US$ 12.00 </strong>
                                <a href="#" class="action"><i class="icon-trash"></i></a>
                            </li>
                            <li>
                                <div>Total: <span>US$ 12.00</span></div>
                                <a href="deseos.php" class="button_drop">Ir a Deseos</a>
                                <a href="cart.php" class="button_drop outline">Contratar</a>
                            </li>                            
                        </ul>
                    </div><!-- End dropdown-cart-->
                </li>
            </ul>
        </nav>
    </div>
</div><!-- container -->