<!DOCTYPE html>
<html lang="en">

<head>
      <?php require 'header.php';?>
</head>

<header>
    <div id="top_line">
        <?php require 'header1.php';?>
    </div>
         <?php require 'header2.php';?>  
</header>

	<section class="parallax-window" data-parallax="scroll" data-image-src="img/home_bg_1.jpg" data-natural-width="1400" data-natural-height="470">
		<div class="parallax-content-1">
			<div class="animated fadeInDown">
				<h1>Preguntas Frecuentes</h1>
				<p>Ridiculus sociosqu cursus neque cursus curae ante scelerisque vehicula.</p>
			</div>
		</div>
	</section>
	<!-- End section -->

	<main>
		<div id="position">
			<div class="container">
			</div>
		</div>
		<!-- Position -->

		<div class="container margin_60">

			<div class="row">
				<aside class="col-lg-3">
					<div class="box_style_cat">
						<ul id="cat_nav">
							<li><a href="#" class="active"><i class="icon_set_1_icon-95"></i>Pagos</a>
							</li>
							<li><a href="#"><i class="icon_set_1_icon-95"></i>Guías</a>
							</li>
							<li><a href="#"><i class="icon_set_1_icon-95"></i>Recomendaciones de Viaje</a>
							</li>
							<li><a href="#"><i class="icon_set_1_icon-95"></i>Términos y Condiciones</a>
							</li>
							<li><a href="#"><i class="icon_set_1_icon-95"></i>Servicios</a>
							</li>
							<li><a href="#"><i class="icon_set_1_icon-95"></i>Transfers</a>
							</li>
						</ul>
					</div>

					<div class="box_style_2">
                        <?php require 'telefono.php';?>
					</div>
				</aside>
				<!--End aside -->
				<div class="col-lg-9" id="faq">
					<h3 class="nomargin_top">Pagos</h3>
					<div id="payment" class="accordion_styled">
					  <div class="card">
						<div class="card-header">
						  <h4>
							<a class="accordion-toggle" data-toggle="collapse" data-parent="#payment" href="#collapseOne_payment">Anim pariatur cliche reprehenderit?<i class="indicator icon-minus float-right"></i></a>
						  </h4>
						</div>
						<div id="collapseOne_payment" class="collapse show" data-parent="#payment">
						  <div class="card-body">
							Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
						  </div>
						</div>
					  </div>
					  <div class="card">
						<div class="card-header">
						  <h4>
							<a class="accordion-toggle" data-toggle="collapse" data-parent="#payment" href="#collapseTwo_payment">Parsnip lotus root celery?<i class="indicator icon-plus float-right"></i></a>
						  </h4>
						</div>
						<div id="collapseTwo_payment" class="collapse" data-parent="#payment">
						  <div class="card-body">
							Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
						  </div>
						</div>
					  </div>
					  <div class="card">
						<div class="card-header">
						  <h4>
							<a class="accordion-toggle" data-toggle="collapse" data-parent="#payment" href="#collapseThree_payment">Beet greens peanut salad?<i class="indicator icon-plus float-right"></i></a>
						  </h4>
						</div>
						<div id="collapseThree_payment" class="collapse" data-parent="#payment">
						  <div class="card-body">
							Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
						  </div>
						</div>
					  </div>
					</div>
					<!--End payment -->

					<h3>Guías</h3>
					<div id="tips" class="accordion_styled">
					  <div class="card">
						<div class="card-header">
						  <h4>
							<a class="accordion-toggle" data-toggle="collapse" data-parent="#tips" href="#collapseOne_tips">Anim pariatur cliche reprehenderit?<i class="indicator icon-plus float-right"></i></a>
						  </h4>
						</div>
						<div id="collapseOne_tips" class="collapse" data-parent="#tips">
						  <div class="card-body">
							Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
						  </div>
						</div>
					  </div>
					  <div class="card">
						<div class="card-header">
						  <h4>
							<a class="accordion-toggle" data-toggle="collapse" data-parent="#tips" href="#collapseTwo_tips">Parsnip lotus root celery?<i class="indicator icon-plus float-right"></i></a>
						  </h4>
						</div>
						<div id="collapseTwo_tips" class="collapse" data-parent="#tips">
						  <div class="card-body">
							Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
						  </div>
						</div>
					  </div>
					  <div class="card">
						<div class="card-header">
						  <h4>
							<a class="accordion-toggle" data-toggle="collapse" data-parent="#tips" href="#collapseThree_tips">Beet greens peanut salad?<i class="indicator icon-plus float-right"></i></a>
						  </h4>
						</div>
						<div id="collapseThree_tips" class="collapse" data-parent="#tips">
						  <div class="card-body">
							Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
						  </div>
						</div>
					  </div>
					</div>
					<!--End tips -->

					<h3>Recomendaciones de Viaje</h3>
					<div id="reccomendations" class="accordion_styled">
					  <div class="card">
						<div class="card-header">
						  <h4>
							<a class="accordion-toggle" data-toggle="collapse" data-parent="#reccomendations" href="#collapseOne_reccomendations">Anim pariatur cliche reprehenderit?<i class="indicator icon-plus float-right"></i></a>
						  </h4>
						</div>
						<div id="collapseOne_reccomendations" class="collapse" data-parent="#reccomendations">
						  <div class="card-body">
							Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
						  </div>
						</div>
					  </div>
					  <div class="card">
						<div class="card-header">
						  <h4>
							<a class="accordion-toggle" data-toggle="collapse" data-parent="#reccomendations" href="#collapseTwo_reccomendations">Parsnip lotus root celery?<i class="indicator icon-plus float-right"></i></a>
						  </h4>
						</div>
						<div id="collapseTwo_reccomendations" class="collapse" data-parent="#reccomendations">
						  <div class="card-body">
							Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
						  </div>
						</div>
					  </div>
					  <div class="card">
						<div class="card-header">
						  <h4>
							<a class="accordion-toggle" data-toggle="collapse" data-parent="#reccomendations" href="#collapseThree_reccomendations">Beet greens peanut salad?<i class="indicator icon-plus float-right"></i></a>
						  </h4>
						</div>
						<div id="collapseThree_reccomendations" class="collapse" data-parent="#reccomendations">
						  <div class="card-body">
							Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
						  </div>
						</div>
					  </div>
					</div>
					<!--End reccomendations -->

					<h3>Términos y Condiciones</h3>
					<div id="terms" class="accordion_styled">
					  <div class="card">
						<div class="card-header">
						  <h4>
							<a class="accordion-toggle" data-toggle="collapse" data-parent="#terms" href="#collapseOne_terms">Anim pariatur cliche reprehenderit?<i class="indicator icon-plus float-right"></i></a>
						  </h4>
						</div>
						<div id="collapseOne_terms" class="collapse" data-parent="#terms">
						  <div class="card-body">
							Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
						  </div>
						</div>
					  </div>
					  <div class="card">
						<div class="card-header">
						  <h4>
							<a class="accordion-toggle" data-toggle="collapse" data-parent="#terms" href="#collapseTwo_terms">Parsnip lotus root celery?<i class="indicator icon-plus float-right"></i></a>
						  </h4>
						</div>
						<div id="collapseTwo_terms" class="collapse" data-parent="#terms">
						  <div class="card-body">
							Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
						  </div>
						</div>
					  </div>
					  <div class="card">
						<div class="card-header">
						  <h4>
							<a class="accordion-toggle" data-toggle="collapse" data-parent="#terms" href="#collapseThree_terms">Beet greens peanut salad?<i class="indicator icon-plus float-right"></i></a>
						  </h4>
						</div>
						<div id="collapseThree_terms" class="collapse" data-parent="#terms">
						  <div class="card-body">
							Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
						  </div>
						</div>
					  </div>
					</div>
					<!--End terms -->

					<h3>Servicios</h3>
					<div id="booking" class="accordion_styled">
					  <div class="card">
						<div class="card-header">
						  <h4>
							<a class="accordion-toggle" data-toggle="collapse" data-parent="#booking" href="#collapseOne_booking">Anim pariatur cliche reprehenderit?<i class="indicator icon-plus float-right"></i></a>
						  </h4>
						</div>
						<div id="collapseOne_booking" class="collapse" data-parent="#booking">
						  <div class="card-body">
							Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
						  </div>
						</div>
					  </div>
					  <div class="card">
						<div class="card-header">
						  <h4>
							<a class="accordion-toggle" data-toggle="collapse" data-parent="#booking" href="#collapseTwo_booking">Parsnip lotus root celery?<i class="indicator icon-plus float-right"></i></a>
						  </h4>
						</div>
						<div id="collapseTwo_booking" class="collapse" data-parent="#booking">
						  <div class="card-body">
							Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
						  </div>
						</div>
					  </div>
					  <div class="card">
						<div class="card-header">
						  <h4>
							<a class="accordion-toggle" data-toggle="collapse" data-parent="#booking" href="#collapseThree_booking">Beet greens peanut salad?<i class="indicator icon-plus float-right"></i></a>
						  </h4>
						</div>
						<div id="collapseThree_booking" class="collapse" data-parent="#booking">
						  <div class="card-body">
							Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
						  </div>
						</div>
					  </div>
					</div>
					<!--End booking -->

					<h3>Transfers</h3>
					<div id="transfers" class="accordion_styled">
					  <div class="card">
						<div class="card-header">
						  <h4>
							<a class="accordion-toggle" data-toggle="collapse" data-parent="#transfers" href="#collapseOne_transfers">Anim pariatur cliche reprehenderit?<i class="indicator icon-plus float-right"></i></a>
						  </h4>
						</div>
						<div id="collapseOne_transfers" class="collapse" data-parent="#transfers">
						  <div class="card-body">
							Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
						  </div>
						</div>
					  </div>
					  <div class="card">
						<div class="card-header">
						  <h4>
							<a class="accordion-toggle" data-toggle="collapse" data-parent="#transfers" href="#collapseTwo_transfers">Parsnip lotus root celery?<i class="indicator icon-plus float-right"></i></a>
						  </h4>
						</div>
						<div id="collapseTwo_transfers" class="collapse" data-parent="#transfers">
						  <div class="card-body">
							Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
						  </div>
						</div>
					  </div>
					  <div class="card">
						<div class="card-header">
						  <h4>
							<a class="accordion-toggle" data-toggle="collapse" data-parent="#transfers" href="#collapseThree_transfers">Beet greens peanut salad?<i class="indicator icon-plus float-right"></i></a>
						  </h4>
						</div>
						<div id="collapseThree_transfers" class="collapse" data-parent="#transfers">
						  <div class="card-body">
							Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
						  </div>
						</div>
					  </div>
					</div>
					<!--End transfers -->

					<h3>Precios</h3>
					<div id="pricing" class="accordion_styled">
					  <div class="card">
						<div class="card-header">
						  <h4>
							<a class="accordion-toggle" data-toggle="collapse" data-parent="#pricing" href="#collapseOne_pricing">Anim pariatur cliche reprehenderit?<i class="indicator icon-plus float-right"></i></a>
						  </h4>
						</div>
						<div id="collapseOne_pricing" class="collapse" data-parent="#pricing">
						  <div class="card-body">
							Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
						  </div>
						</div>
					  </div>
					  <div class="card">
						<div class="card-header">
						  <h4>
							<a class="accordion-toggle" data-toggle="collapse" data-parent="#pricing" href="#collapseTwo_pricing">Parsnip lotus root celery?<i class="indicator icon-plus float-right"></i></a>
						  </h4>
						</div>
						<div id="collapseTwo_pricing" class="collapse" data-parent="#pricing">
						  <div class="card-body">
							Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
						  </div>
						</div>
					  </div>
					  <div class="card">
						<div class="card-header">
						  <h4>
							<a class="accordion-toggle" data-toggle="collapse" data-parent="#pricing" href="#collapseThree_pricing">Beet greens peanut salad?<i class="indicator icon-plus float-right"></i></a>
						  </h4>
						</div>
						<div id="collapseThree_pricing" class="collapse" data-parent="#pricing">
						  <div class="card-body">
							Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
						  </div>
						</div>
					  </div>
					</div>
					<!--End pricing -->

					<h3>Privacidad</h3>
					<div id="privacy" class="accordion_styled">
					  <div class="card">
						<div class="card-header">
						  <h4>
							<a class="accordion-toggle" data-toggle="collapse" data-parent="#privacy" href="#collapseOne_privacy">Anim pariatur cliche reprehenderit?<i class="indicator icon-plus float-right"></i></a>
						  </h4>
						</div>
						<div id="collapseOne_privacy" class="collapse" data-parent="#privacy">
						  <div class="card-body">
							Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
						  </div>
						</div>
					  </div>
					  <div class="card">
						<div class="card-header">
						  <h4>
							<a class="accordion-toggle" data-toggle="collapse" data-parent="#privacy" href="#collapseTwo_privacy">Parsnip lotus root celery?<i class="indicator icon-plus float-right"></i></a>
						  </h4>
						</div>
						<div id="collapseTwo_privacy" class="collapse" data-parent="#privacy">
						  <div class="card-body">
							Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
						  </div>
						</div>
					  </div>
					  <div class="card">
						<div class="card-header">
						  <h4>
							<a class="accordion-toggle" data-toggle="collapse" data-parent="#privacy" href="#collapseThree_privacy">Beet greens peanut salad?<i class="indicator icon-plus float-right"></i></a>
						  </h4>
						</div>
						<div id="collapseThree_privacy" class="collapse" data-parent="#privacy">
						  <div class="card-body">
							Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
						  </div>
						</div>
					  </div>
					</div>
					<!--End privacy -->
				</div>
				<!-- End col lg-9 -->
			</div>
			<!-- End row -->
		</div>
		<!-- End container -->
	</main>
	<!-- End main -->
            <?php require 'footer.php';?>

</body>

</html>