	<footer class="revealed">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <h3>¿Necesita Ayuda?</h3>
                    <a href="tel://+54 91165309188" id="phone">+54 11 65309188</a>
                    <a href="mailto:help@citytours.com" id="email_footer">contacto@bestguide.com.ar</a>
                </div>
                <div class="col-md-3">
                    <h3>Acerca</h3>
                    <ul>
                        <li><a href="#">Scerca de Nosotros</a></li>
                        <li><a href="#">Preguntas Frecuentes</a></li>
                        <li><a href="#">Conectarse</a></li>
                        <li><a href="#">Registrarse</a></li>
                         <li><a href="#">Terminos y Condiciones</a></li>
                    </ul>
                </div>
                <div class="col-md-3">
                    <h3>Navegar</h3>
                    <ul>
                        <li><a href="#">Lista de Deseos</a></li>
                         <li><a href="#">Galería</a></li>
                    </ul>
                </div>
                <div class="col-md-2">
                    <h3>Configuración</h3>
                    <div class="styled-select">
                        <select name="lang" id="lang">
                            <option value="English" selected>English</option>
                            <option value="French">French</option>
                            <option value="Spanish">Español</option>
                            <option value="Russian">Russian</option>
                        </select>
                    </div>
                    <div class="styled-select">
                        <select name="currency" id="currency">
                            <option value="USD" selected>USD</option>
                            <option value="EUR">EUR</option>
                            <option value="GBP">GBP</option>
                            <option value="RUB">RUB</option>
                        </select>
                    </div>
                </div>
            </div><!-- End row -->
            <div class="row">
                <div class="col-md-12">
                    <div id="social_footer">
                        <ul>
                            <li><a href="#"><i class="icon-facebook"></i></a></li>
                            <li><a href="#"><i class="icon-twitter"></i></a></li>
                            <li><a href="#"><i class="icon-google"></i></a></li>
                            <li><a href="#"><i class="icon-instagram"></i></a></li>
                            <li><a href="#"><i class="icon-pinterest"></i></a></li>
                            <li><a href="#"><i class="icon-vimeo"></i></a></li>
                            <li><a href="#"><i class="icon-youtube-play"></i></a></li>
                        </ul>
                        <p>© BestGuide 2019</p>
                    </div>
                </div>
            </div><!-- End row -->
        </div><!-- End container -->
    </footer><!-- End footer -->

	<div id="toTop"></div><!-- Back to top button -->
	
	<!-- Search Menu -->
	<div class="search-overlay-menu">
		<span class="search-overlay-close"><i class="icon_set_1_icon-77"></i></span>
		<form role="search" id="searchform" method="get">
			<input value="" name="q" type="search" placeholder="Search..." />
			<button type="submit"><i class="icon_set_1_icon-78"></i>
			</button>
		</form>
	</div><!-- End Search Menu -->
	
	<!-- Sign In Popup -->
	<div id="sign-in-dialog" class="zoom-anim-dialog mfp-hide">
		<div class="small-dialog-header">
			<h3>Sign In</h3>
		</div>
		<form>
			<div class="sign-in-wrapper">
				<a href="#0" class="social_bt facebook">Login con Facebook</a>
				<a href="#0" class="social_bt google">Login con Google</a>
				<div class="divider"><span>Or</span></div>
				<div class="form-group">
					<label>Email</label>
					<input type="email" class="form-control" name="email" id="email" value="wpisacco@gmail.com">
					<i class="icon_mail_alt"></i>
				</div>
				<div class="form-group">
					<label>Password</label>
					<input type="password" class="form-control" name="password" id="password" value=">
					<i class="icon_lock_alt"></i>
				</div>
				<div class="clearfix add_bottom_15">
					<div class="checkboxes float-left">
						<input id="remember-me" type="checkbox" name="check">
						<label for="remember-me">Recordarme</label>
					</div>
					<div class="float-right"><a id="forgot" href="javascript:void(0);">¿Perdíste tu Password?</a></div>
				</div>
				<div class="text-center"><input type="submit" value="Log In" class="btn_login"></div>
				<div class="text-center">
					¿No tienes una cuenta? <a href="javascript:void(0);">Ingresar</a>
				</div>
				<div id="forgot_pw">
					<div class="form-group">
						<label>Por favor confirme su correo</label>
						<input type="email" class="form-control" name="email_forgot" id="email_forgot">
						<i class="icon_mail_alt"></i>
					</div>
					<p>Recibirá un correo con un link para cambiar su contraseña.</p>
					<div class="text-center"><input type="submit" value="Reset Password" class="btn_1"></div>
				</div>
			</div>
		</form>
		<!--form -->
	</div>
	<!-- /Sign In Popup -->

	<!-- Common scripts -->
	<script src="js/jquery-2.2.4.min.js"></script>
	<script src="js/common_scripts_min.js"></script>
	<script src="js/functions.js"></script>

	<script>
		//Search bar
		$(function() {
			"use strict";
			$("#searchDropdownBox").change(function() {
				var Search_Str = $(this).val();
				//replace search str in span value
				$("#nav-search-in-content").text(Search_Str);
			});
		});



	<!-- Date and time pickers checks -->
		$('input.date-pick').datepicker('setDate', 'today');
		$('input.time-pick').timepicker({minuteStep: 15,showInpunts: false});

	</script>