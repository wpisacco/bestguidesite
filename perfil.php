<!DOCTYPE html>
<html lang="sp">

<head>
            <?php require 'header.php';?>
</head>

<header>
    <div id="top_line">
        <?php require 'header1.php';?>

    </div>
         <?php require 'header2.php';?>       
</header>

<body>
	<section class="parallax-window" data-parallax="scroll" data-image-src="img/single_tour_bg_1.jpg" data-natural-width="1400" data-natural-height="470">
		<div class="parallax-content-2">
			<div class="container">
				<div class="row">
					<div class="col-md-8">
						<h1>Cecilia</h1>
						<span>Buenos Aires, Argentina.</span>
						<span class="rating"><i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile"></i><small>(22)</small></span>
					</div>
					<div class="col-md-4">
						<div id="price_single_main">
							por hora /desde <span><sup>US$</sup> 8</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- End section -->

	<main>
		<div id="position">
			<div class="container">
				<ul>
				</ul>
			</div>
		</div>
		<!-- End Position -->

		<div class="collapse" id="collapseMap">
			<div id="map" class="map"></div>
		</div>
		<!-- End Map -->

		<div class="container margin_60">
			<div class="row">
				<div class="col-lg-8" id="single_tour_desc">

					<div id="single_tour_feat">
						<ul>
							<li><i class="icon_set_1_icon-83"></i>4 Hours</li>
							<li><i class="icon_set_1_icon-50"></i>Shopping</li>
							<li><i class="icon_set_1_icon-82"></i>144 Likes</li>
							<li><i class="icon_set_1_icon-22"></i>Mascotas permitidas</li>
							<li><i class="icon_set_1_icon-40"></i>Bicicleta</li>
							<li><i class="icon_set_1_icon-30"></i>Caminatas</li>
                            <li><i class="icon_set_1_icon-58"></i>Comidas</li>                            
						</ul>
					</div>

					<div class="row">
						<div class="col-lg-3">
							<h3>Descripcion</h3>
						</div>
						<div class="col-lg-9">
							<h4>Buenos Aires, Paris de America </h4>
							<p>
								Lorem ipsum dolor sit amet, at omnes deseruisse pri. Quo aeterno legimus insolens ad. Sit cu detraxit constituam, an mel iudico constituto efficiendi. Eu ponderum mediocrem has, vitae adolescens in pro. Mea liber ridens inermis ei, mei legendos vulputate an, labitur tibique te qui.
							</p>
							<h4>Que incluye</h4>
							<div class="row">
								<div class="col-md-6">
									<ul class="list_ok">
										<li>Lorem ipsum dolor sit amet</li>
										<li>No scripta electram necessitatibus sit</li>
										<li>Quidam percipitur instructior an eum</li>
										<li>Ut est saepe munere ceteros</li>
										<li>No scripta electram necessitatibus sit</li>
										<li>Quidam percipitur instructior an eum</li>
									</ul>
								</div>
								<div class="col-md-6">
									<ul class="list_ok">
										<li>Lorem ipsum dolor sit amet</li>
										<li>No scripta electram necessitatibus sit</li>
										<li>Quidam percipitur instructior an eum</li>
										<li>No scripta electram necessitatibus sit</li>
									</ul>
								</div>
							</div>
							<!-- End row  -->
						</div>
					</div>

					<hr>

					<div class="row">
						<div class="col-lg-3">
							<h3>Galería</h3>
						</div>
						<div class="col-lg-9">


        <div class="container margin_60">
            <div id="gallery" class="final-tiles-gallery effect-dezoom effect-fade-out caption-top social-icons-right">
                <div class="ftg-items">

                    <div class="tile">
                        <a class="tile-inner" data-title="Lorem ipsum" data-lightbox="gallery" href="img/perfiles/ceci/accessories-84528_1920.jpg">
                            <img class="item" src="img/perfiles/ceci/accessories-84528_1920.jpg" data-src="img/perfiles/ceci/accessories-84528_1920.jpg" alt="Image">
                            <span class='title'>Lorem ipsum</span>
                            <span class='subtitle'>Pellentesque ornare ornare</span>
                        </a>
                        <div class="ftg-social">
                            <a href="#" data-social="twitter"><i class="fa fa-twitter"></i></a>
                            <a href="#" data-social="facebook"><i class="fa fa-facebook"></i></a>
                            <a href="#" data-social="google-plus"><i class="fa fa-google"></i></a>
                            <a href="#" data-social="pinterest"><i class="fa fa-pinterest"></i></a>
                        </div>
                    </div>
                    <!-- End image -->

                    <div class="tile">
                        <a class="tile-inner" data-title="Lorem ipsum" data-lightbox="gallery" href="http://placehold.it/800x600/B52626/ffffff&text=1">
                            <img class="item" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-src="http://placehold.it/250x250/cccccc/ffffff&text=250x250" alt="Image">
                            <span class='title'>Lorem ipsum</span>
                            <span class='subtitle'>Pellentesque ornare ornare</span>
                        </a>
                        <div class="ftg-social">
                            <a href="#" data-social="twitter"><i class="fa fa-twitter"></i></a>
                            <a href="#" data-social="facebook"><i class="fa fa-facebook"></i></a>
                            <a href="#" data-social="google-plus"><i class="fa fa-google"></i></a>
                            <a href="#" data-social="pinterest"><i class="fa fa-pinterest"></i></a>
                        </div>
                    </div>
                    <!-- End image -->


                    <div class="tile">
                        <a class="tile-inner" data-title="Lorem ipsum" data-lightbox="gallery" href="http://placehold.it/800x600/B52626/ffffff&text=1">
                            <img class="item" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-src="http://placehold.it/400x800/cccccc/ffffff&text=400x800" alt="Image">
                            <span class='title'>Lorem ipsum</span>
                            <span class='subtitle'>Pellentesque ornare ornare</span>
                        </a>
                        <div class="ftg-social">
                            <a href="#" data-social="twitter"><i class="fa fa-twitter"></i></a>
                            <a href="#" data-social="facebook"><i class="fa fa-facebook"></i></a>
                            <a href="#" data-social="google-plus"><i class="fa fa-google"></i></a>
                            <a href="#" data-social="pinterest"><i class="fa fa-pinterest"></i></a>
                        </div>
                    </div>
                    <!-- End image -->

                    <div class="tile">
                        <a class="tile-inner" data-title="Lorem ipsum" data-lightbox="gallery" href="http://placehold.it/800x600/B52626/ffffff&text=1">
                            <img class="item" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-src="http://placehold.it/600x400/cccccc/ffffff&text=600x400" alt="Image">
                            <span class='title'>Lorem ipsum</span>
                            <span class='subtitle'>Pellentesque ornare ornare</span>
                        </a>
                        <div class="ftg-social">
                            <a href="#" data-social="twitter"><i class="fa fa-twitter"></i></a>
                            <a href="#" data-social="facebook"><i class="fa fa-facebook"></i></a>
                            <a href="#" data-social="google-plus"><i class="fa fa-google"></i></a>
                            <a href="#" data-social="pinterest"><i class="fa fa-pinterest"></i></a>
                        </div>
                    </div>
                    <!-- End image -->

                    <div class="tile">
                        <a class="tile-inner" data-title="Lorem ipsum" data-lightbox="gallery" href="http://placehold.it/800x600/B52626/ffffff&text=1">
                            <img class="item" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-src="http://placehold.it/250x600/cccccc/ffffff&text=250x600" alt="Image">
                            <span class='title'>Lorem ipsum</span>
                            <span class='subtitle'>Pellentesque ornare ornare</span>
                        </a>
                        <div class="ftg-social">
                            <a href="#" data-social="twitter"><i class="fa fa-twitter"></i></a>
                            <a href="#" data-social="facebook"><i class="fa fa-facebook"></i></a>
                            <a href="#" data-social="google-plus"><i class="fa fa-google"></i></a>
                            <a href="#" data-social="pinterest"><i class="fa fa-pinterest"></i></a>
                        </div>
                    </div>
                    <!-- End image -->

                    <div class="tile">
                        <a class="tile-inner" data-title="Lorem ipsum" data-lightbox="gallery" href="http://placehold.it/800x600/B52626/ffffff&text=1">
                            <img class="item" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-src="http://placehold.it/800x800/cccccc/ffffff&text=800x800" alt="Image">
                            <span class='title'>Lorem ipsum</span>
                            <span class='subtitle'>Pellentesque ornare ornare</span>
                        </a>
                        <div class="ftg-social">
                            <a href="#" data-social="twitter"><i class="fa fa-twitter"></i></a>
                            <a href="#" data-social="facebook"><i class="fa fa-facebook"></i></a>
                            <a href="#" data-social="google-plus"><i class="fa fa-google"></i></a>
                            <a href="#" data-social="pinterest"><i class="fa fa-pinterest"></i></a>
                        </div>
                    </div>
                    <!-- End image -->

                    <div class="tile">
                        <a class="tile-inner" data-title="Lorem ipsum" data-lightbox="gallery" href="http://placehold.it/800x600/B52626/ffffff&text=1">
                            <img class="item" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-src="http://placehold.it/800x600/cccccc/ffffff&text=800x600" alt="Image">
                            <span class='title'>Lorem ipsum</span>
                            <span class='subtitle'>Pellentesque ornare ornare</span>
                        </a>
                        <div class="ftg-social">
                            <a href="#" data-social="twitter"><i class="fa fa-twitter"></i></a>
                            <a href="#" data-social="facebook"><i class="fa fa-facebook"></i></a>
                            <a href="#" data-social="google-plus"><i class="fa fa-google"></i></a>
                            <a href="#" data-social="pinterest"><i class="fa fa-pinterest"></i></a>
                        </div>
                    </div>
                    <!-- End image -->

                    <div class="tile">
                        <a class="tile-inner" data-title="Lorem ipsum" data-lightbox="gallery" href="http://placehold.it/800x600/B52626/ffffff&text=1">
                            <img class="item" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-src="http://placehold.it/250x300/cccccc/ffffff&text=250x300" alt="Image">
                            <span class='title'>Lorem ipsum</span>
                            <span class='subtitle'>Pellentesque ornare ornare</span>
                        </a>
                        <div class="ftg-social">
                            <a href="#" data-social="twitter"><i class="fa fa-twitter"></i></a>
                            <a href="#" data-social="facebook"><i class="fa fa-facebook"></i></a>
                            <a href="#" data-social="google-plus"><i class="fa fa-google"></i></a>
                            <a href="#" data-social="pinterest"><i class="fa fa-pinterest"></i></a>
                        </div>
                    </div>
                    <!-- End image -->

                    <div class="tile">
                        <a class="tile-inner" data-title="Lorem ipsum" data-lightbox="gallery" href="http://placehold.it/800x600/B52626/ffffff&text=1">
                            <img class="item" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-src="http://placehold.it/250x250/cccccc/ffffff&text=250x250" alt="Image">
                            <span class='title'>Lorem ipsum</span>
                            <span class='subtitle'>Pellentesque ornare ornare</span>
                        </a>
                        <div class="ftg-social">
                            <a href="#" data-social="twitter"><i class="fa fa-twitter"></i></a>
                            <a href="#" data-social="facebook"><i class="fa fa-facebook"></i></a>
                            <a href="#" data-social="google-plus"><i class="fa fa-google"></i></a>
                            <a href="#" data-social="pinterest"><i class="fa fa-pinterest"></i></a>
                        </div>
                    </div>
                    <!-- End image -->

                </div>
            </div>

        </div>




						</div>
					</div>

					<hr>

					<div class="row">
						<div class="col-lg-3">
							<h3>Comentarios </h3>
							<a href="#" class="btn_1 add_bottom_30" data-toggle="modal" data-target="#myReview">Comentar</a>
						</div>
						<div class="col-lg-9">
							<div id="general_rating">3 Comenarios
								<div class="rating">
									<i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile"></i><i class="icon-smile"></i>
								</div>
							</div>
							<!-- End general_rating -->
							<div class="row" id="rating_summary">
								<div class="col-md-6">
									<ul>
										<li>Position
											<div class="rating">
												<i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile"></i><i class="icon-smile"></i>
											</div>
										</li>
										<li>Guía
											<div class="rating">
												<i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile"></i>
											</div>
										</li>
									</ul>
								</div>
								<div class="col-md-6">
									<ul>
										<li>Precio
											<div class="rating">
												<i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile"></i><i class="icon-smile"></i>
											</div>
										</li>
										<li>Calidad
											<div class="rating">
												<i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile voted"></i>
											</div>
										</li>
									</ul>
								</div>
							</div>
							<!-- End row -->
							<hr>
							<div class="review_strip_single">
								<img src="img/avatar1.jpg" alt="Image" class="rounded-circle">
								<small> - 10 March 2015 -</small>
								<h4>Jhon Doe</h4>
								<p>
									"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed a lorem quis neque interdum consequat ut sed sem. Duis quis tempor nunc. Interdum et malesuada fames ac ante ipsum primis in faucibus."
								</p>
								<div class="rating">
									<i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile"></i><i class="icon-smile"></i>
								</div>
							</div>
							<!-- End review strip -->

							<div class="review_strip_single">
								<img src="img/avatar3.jpg" alt="Image" class="rounded-circle">
								<small> - 10 March 2015 -</small>
								<h4>Jhon Doe</h4>
								<p>
									"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed a lorem quis neque interdum consequat ut sed sem. Duis quis tempor nunc. Interdum et malesuada fames ac ante ipsum primis in faucibus."
								</p>
								<div class="rating">
									<i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile"></i><i class="icon-smile"></i>
								</div>
							</div>
							<!-- End review strip -->

							<div class="review_strip_single last">
								<img src="img/avatar2.jpg" alt="Image" class="rounded-circle">
								<small> - 10 March 2015 -</small>
								<h4>Jhon Doe</h4>
								<p>
									"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed a lorem quis neque interdum consequat ut sed sem. Duis quis tempor nunc. Interdum et malesuada fames ac ante ipsum primis in faucibus."
								</p>
								<div class="rating">
									<i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile"></i><i class="icon-smile"></i>
								</div>
							</div>
							<!-- End review strip -->
						</div>
					</div>
				</div>
				<!--End  single_tour_desc-->

				<aside class="col-lg-4">
					<div class="box_style_1 expose">
						<h3 class="inner">- Calendario -</h3>
						<div class="row">
							<div class="col-sm-6">
								<div class="form-group">
									<label><i class="icon-calendar-7"></i> Elija un día</label>
									<input class="form-control booking_date" type="text" data-lang="en" data-large-mode="true" data-large-default="true" data-min-year="2017" data-max-year="2020" data-disabled-days="11/17/2017,12/17/2017">
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group">
									<label><i class=" icon-clock"></i> Hora Aproximada</label>
                                    <input class="form-control booking_time" type="text">
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-6">
								<div class="form-group">
									<label>Adultos</label>
									<div class="numbers-row">
										<input type="text" value="1" id="adults" class="qty2 form-control" name="quantity">
									</div>
								</div>
							</div>
							<div class="col-6">
								<div class="form-group">
									<label>Niños</label>
									<div class="numbers-row">
										<input type="text" value="0" id="children" class="qty2 form-control" name="quantity">
									</div>
								</div>
							</div>
						</div>
						<br>
						<table class="table table_summary">
							<tbody>
								<tr>
									<td>
										Adultos
									</td>
									<td class="text-right">
										2
									</td>
								</tr>
								<tr>
									<td>
										Niños
									</td>
									<td class="text-right">
										0
									</td>
								</tr>
								<tr>
									<td>
										Costo del Servicio
									</td>
									<td class="text-right">
										3x US$ 8
									</td>
								</tr>
								<tr class="total">
									<td>
										Costo Total
									</td>
									<td class="text-right">
										US$ 30 (*)
									</td>
								</tr>
							</tbody>
						</table>
						<a class="btn_full" href="cart.php">Contratar Ahora</a>
						<a class="btn_full_outline" href="#"><i class=" icon-heart"></i> Agregar a Deseos</a>
					</div>
					<!--/box_style_1 -->

					<div class="box_style_4">
                        <?php require 'telefono.php';?>
					</div>

				</aside>
			</div>
			<!--End row -->
		</div>
		<!--End container -->
		
	<div id="overlay"></div>
	<!-- Mask on input focus -->
		
	</main>
	<!-- End main -->

            <?php require 'footer.php';?>
	

</body>

</html>